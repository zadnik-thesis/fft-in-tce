--===========================================================================--
-- Arbiter for control signals to allow external memory writing
--
-- mem_wr_lock controls the behaviour. When '1', processor core is stopped
-- and dummy rstx and glock signals are provided to memories as if the
-- processor was running. When '0', rstx and locked signals are propagated
-- normally.
--
-- It is intended to be used with parallel memory access logic (par_mem_logic).
-- More info in par_mem_logic_reg.vhdl file or the following article:
--    name:    Parallel Memory Architecture for TTA Processor
--    authors: Jarno K. Tanskanen, Teemu Pitkänen, Risto Mäkinen, Jarmo Takala
--    url:     https://tce.cs.tut.fi/papers/parallel_memory.pdf
--
-- Note that when in the init mode, the lock_rq signal from the par_mem_logic
-- won't be respected (it is fed to the inactive core). Thus, there is no
-- conflict protection. However, the external memory interface is usually
-- connected to only one LSU, so there should not be any conflicts.
--===========================================================================--


--===========================================================================--
-- Entity of control arbiter
-------------------------------------------------------------------------------
-- Inputs:
--    mem_wr_lock  : External signal locking the processor for memory init
--    rstx         : External rstx signal
--    locked       : locked signal from processor core
--
-- Outputs:
--    rstx_proc    : rstx signal to be sent to the processor core
--    rstx_mem     : rstx signal to be sent to the memories
--    glock_mem    : glock signal to be sent to the memories
-------------------------------------------------------------------------------
library IEEE;
use IEEE.Std_Logic_1164.all;

entity control_arbiter is
    port (
        mem_wr_lock : in  std_logic;
        rstx        : in  std_logic;
        locked      : in  std_logic;
        rstx_proc   : out std_logic;
        rstx_mem    : out std_logic;
        glock_mem   : out std_logic);
end control_arbiter;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of control arbiter
-------------------------------------------------------------------------------
architecture Behavioral of control_arbiter is
begin -- Behavioral

    select_control : process(mem_wr_lock, rstx, locked)
    begin --select_control
        if mem_wr_lock = '1' then  -- memory write mode
            rstx_proc <= '0';
            rstx_mem  <= '1';
            glock_mem <= '0';
        else                       -- normal mode
            rstx_proc <= rstx;
            rstx_mem  <= rstx;
            glock_mem <= locked;
        end if;
    end process select_control;

end Behavioral;
--===========================================================================--
