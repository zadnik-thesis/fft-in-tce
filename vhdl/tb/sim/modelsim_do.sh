#!/bin/bash

####
# Variables
####

GENERATED_DIR='../../../proge_out/fft_full_lbuf'
VHDL_DIR='../..'
ENTITY='testbench'
NEXP=10
NEXP_STR=$(printf "%02d" $NEXP)
MEM_SIZE=$((2**($NEXP-1)))   # size of single-port memory to include

# Log files
LOG_DIR='logs'
COMPILE_LOG_FILE=$LOG_DIR/compile_$NEXP_STR.log
SIM_LOG_FILE=$LOG_DIR/sim_$NEXP_STR.log
SAIF_LOG_FILE=$LOG_DIR/saif_$NEXP_STR.log

# Used in saif generation
TARGET_ENT='testbench/dut'
CLK_PERIOD='4' # Needs to match testbench_constants_pkg.vhd, default is 250 MHz
FREQ=$(echo "1000/$CLK_PERIOD" | bc)
SAIF_DIR="saif/$FREQ"mhz
SAIF_FILE="$SAIF_DIR/par_mem_cacti_${NEXP_STR}.saif"

####
# Functions
####

clean()
{
    echo "Cleaning ..."
    rm -rf work
    rm -rf transcript
    rm -rf *.dump
    rm -rf *_log.txt
    rm -rf *.vstf
    rm -rf conflicts.txt
}

compile()
{
    echo "Compiling ..."
    echo ""

    vlib work
    vmap

    # Generated files from ProGe
    # Some are files from this repository's hdb, some are available in the
    # public TCE version, some are internal only.
    vcom ${GENERATED_DIR}/vhdl/constants.vhd                     || exit 1
    vcom ${GENERATED_DIR}/vhdl/tce_util_pkg.vhdl                 || exit 1
    vcom ${GENERATED_DIR}/vhdl/tta0_imem_mau_pkg.vhdl            || exit 1
    vcom ${GENERATED_DIR}/vhdl/tta0_globals_pkg.vhdl             || exit 1
    vcom ${GENERATED_DIR}/vhdl/tta0_params_pkg.vhdl              || exit 1
    vcom ${GENERATED_DIR}/vhdl/add_sub.vhdl                      || exit 1
    vcom ${GENERATED_DIR}/vhdl/util_pkg.vhdl                     || exit 1
    vcom ${GENERATED_DIR}/vhdl/shl_shr.vhdl                      || exit 1
    vcom ${GENERATED_DIR}/vhdl/fu_ldw_stw_always_4.vhd           || exit 1
    vcom ${GENERATED_DIR}/vhdl/tfg_pipe_4.vhd                    || exit 1
    vcom ${GENERATED_DIR}/vhdl/fu_tfg_always_6.vhd               || exit 1
    vcom ${GENERATED_DIR}/vhdl/idx_stage_split.vhd               || exit 1
    vcom ${GENERATED_DIR}/vhdl/lut_addr.vhd                      || exit 1
    vcom ${GENERATED_DIR}/vhdl/radix_2.vhd                       || exit 1
    vcom ${GENERATED_DIR}/vhdl/scale_k.vhd                       || exit 1
    vcom ${GENERATED_DIR}/vhdl/distr_2049x32_synch.vhd           || exit 1
    vcom ${GENERATED_DIR}/vhdl/tf_lut_synch.vhd                  || exit 1
    vcom ${GENERATED_DIR}/vhdl/twiddle_twist.vhd                 || exit 1
    vcom ${GENERATED_DIR}/vhdl/weight.vhd                        || exit 1
    vcom ${GENERATED_DIR}/vhdl/ag.vhd                            || exit 1
    vcom ${GENERATED_DIR}/vhdl/fu_ag_always_2.vhd                || exit 1
    vcom ${GENERATED_DIR}/vhdl/cmul.vhd                          || exit 1
    vcom ${GENERATED_DIR}/vhdl/fu_cmul_always_3.vhd              || exit 1
    vcom ${GENERATED_DIR}/vhdl/cadd.vhd                          || exit 1
    vcom ${GENERATED_DIR}/vhdl/fu_cadd_always_1.vhd              || exit 1
    vcom ${GENERATED_DIR}/vhdl/rotreg.vhd                        || exit 1
    vcom ${GENERATED_DIR}/vhdl/fu_rotreg_always_11.vhd           || exit 1
    vcom ${GENERATED_DIR}/vhdl/rf_1wr_1rd_always_1_guarded_0.vhd || exit 1
    vcom ${GENERATED_DIR}/vhdl/tta0.vhdl                         || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/gcu_opcodes_pkg.vhdl            || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/datapath_gate.vhdl              || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/decoder.vhdl                    || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/output_socket_3_1.vhdl          || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/idecompressor.vhdl              || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/ifetch.vhdl                     || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/input_socket_1.vhdl             || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/input_socket_2.vhdl             || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/loopbuffer.vhdl                 || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/output_socket_1_1.vhdl          || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/output_socket_2_1.vhdl          || exit 1
    vcom ${GENERATED_DIR}/gcu_ic/ic.vhdl                         || exit 1

    # Constants first...
    vcom ${VHDL_DIR}/proc/proc_constants_pkg.vhd                 || exit 1
    vcom ${VHDL_DIR}/mem/mem_constants_pkg.vhd                   || exit 1
    vcom ${VHDL_DIR}/tb/testbench_constants_pkg.vhd              || exit 1

    # Instruction memory
    vlog ${VHDL_DIR}/mem/cacti_mem/ram_64x51.v                   || exit 1
    # Data memory
    # vlog ${VHDL_DIR}/mem/cacti_mem/ram_${MEM_SIZE}x32.v          || exit 1
    vlog ${VHDL_DIR}/mem/cacti_mem/ram_32x32.v                   || exit 1
    vlog ${VHDL_DIR}/mem/cacti_mem/ram_64x32.v                   || exit 1
    vlog ${VHDL_DIR}/mem/cacti_mem/ram_128x32.v                  || exit 1
    vlog ${VHDL_DIR}/mem/cacti_mem/ram_256x32.v                  || exit 1
    vlog ${VHDL_DIR}/mem/cacti_mem/ram_512x32.v                  || exit 1
    vlog ${VHDL_DIR}/mem/cacti_mem/ram_1024x32.v                 || exit 1
    vlog ${VHDL_DIR}/mem/cacti_mem/ram_2048x32.v                 || exit 1
    vlog ${VHDL_DIR}/mem/cacti_mem/ram_4096x32.v                 || exit 1
    vlog ${VHDL_DIR}/mem/cacti_mem/ram_8192x32.v                 || exit 1
    # Memory utils and wrappers
    vcom ${VHDL_DIR}/mem/block_en_wr_arbiter.vhd                 || exit 1
    vcom ${VHDL_DIR}/mem/block_mem_output_sel.vhd                || exit 1
    vcom ${VHDL_DIR}/mem/ser_par_conv.vhd                        || exit 1
    vcom ${VHDL_DIR}/mem/par_ser_conv.vhd                        || exit 1
    vcom ${VHDL_DIR}/mem/par_mem_logic_reg.vhd                   || exit 1
    vcom ${VHDL_DIR}/mem/par_mem_scheduler.vhd                   || exit 1
    vcom ${VHDL_DIR}/mem/cacti_mem_wrapper.vhd                   || exit 1
    vcom ${VHDL_DIR}/mem/synch_sram.vhd                          || exit 1
    vcom ${VHDL_DIR}/mem/synch_dualport_sram.vhd                 || exit 1
    vcom ${VHDL_DIR}/mem/synch_singleport_mem_wrapper.vhd        || exit 1
    vcom ${VHDL_DIR}/mem/synch_dualport_mem_wrapper.vhd          || exit 1
    vcom ${VHDL_DIR}/mem/imem_arbiter.vhd                        || exit 1
    vcom ${VHDL_DIR}/mem/mem_arbiter.vhd                         || exit 1

    # Synthesizable processor's top module
    vcom ${VHDL_DIR}/proc/control_arbiter.vhd                    || exit 1
    vcom ${VHDL_DIR}/proc/proc.vhd                               || exit 1

    # Testbench - not synthesizable
    vcom ${VHDL_DIR}/tb/clkgen.vhd                               || exit 1
    vcom ${VHDL_DIR}/tb/testbench.vhd                            || exit 1

    echo ""
    echo "Finished compiling."
}

simulate()
{
    echo "Simulating ..."
    DO='run 2000 us; exit'
    OPTS='-c -t 1ps'
    vsim $ENTITY $OPTS -do "$DO"
}

saif()
{
    echo "Generating saif file for NEXP = $NEXP"
    # INIT_ and FINISH_TIME are start and end of the computation, without
    # counting memory init and memory dump to files.
    # Values are taken from Vivado simulation and they are in nanoseconds.
    # They are taken from 250 MHz run. Other frequencies are derived from that.
    case $NEXP in
        6)
            INIT_REF_TIME=448
            FINISH_REF_TIME=1292
            ;;
        7)
            INIT_REF_TIME=704
            FINISH_REF_TIME=2828
            ;;
        8)
            INIT_REF_TIME=1216
            FINISH_REF_TIME=5388
            ;;
        9)
            INIT_REF_TIME=2240
            FINISH_REF_TIME=12556
            ;;
        10)
            INIT_REF_TIME=4288
            FINISH_REF_TIME=24844
            ;;
        11)
            INIT_REF_TIME=8384
            FINISH_REF_TIME=57612
            ;;
        12)
            INIT_REF_TIME=16576
            FINISH_REF_TIME=114956
            ;;
        13)
            INIT_REF_TIME=32960
            FINISH_REF_TIME=262412
            ;;
        14)
            INIT_REF_TIME=65728
            FINISH_REF_TIME=524556
            ;;
        *)
            echo "Invalid NEXP."
            exit 1
            ;;
    esac

    # Reference frequency is 250 MHz
    CLK_REF_PERIOD=4
    CLK_COEF=$(echo "scale=3; $CLK_PERIOD/$CLK_REF_PERIOD" | bc)

    # Scale to the reference frequency (/1 is to remove numbers after decimal)
    INIT_TIME=$(echo "$INIT_REF_TIME*$CLK_COEF/1" | bc)
    FINISH_TIME=$(echo "$FINISH_REF_TIME*$CLK_COEF/1" | bc)

    RUN_TIME=$(echo "$FINISH_TIME-$INIT_TIME" | bc)

    mkdir -p $SAIF_DIR
    vsim -c -t 1ps -do "run $INIT_TIME ns; power add -in -out -inout -internal -r $TARGET_ENT/*; run $RUN_TIME ns; power report -all -bsaif $SAIF_FILE; quit" $ENTITY
}

####
# Main script
####

exclude_0='in an arithmetic operand'
exclude_1='NUMERIC_STD'
exclude_2='CONV_INTEGER:'
exclude_3='Time:'

if [ "$1" = "sim" ]; then
    mkdir -p $LOG_DIR
    simulate 2>&1 | grep -v "${exclude_0}" \
                  | grep -v "${exclude_1}" \
                  | grep -v "${exclude_2}" \
                  | grep -v "${exclude_3}" > $SIM_LOG_FILE
elif [ "$1" = "saif" ]; then
    mkdir -p $LOG_DIR
    saif 2>&1 | grep -v "${exclude_0}" \
              | grep -v "${exclude_1}" \
              | grep -v "${exclude_2}" \
              | grep -v "${exclude_3}" > $SAIF_LOG_FILE
elif [ "$1" = "clean" ]; then
    clean
elif [ "$1" = "compile" ]; then
    clean
    mkdir -p $LOG_DIR
    compile &> $COMPILE_LOG_FILE
elif [ $# -eq 0 ]; then
    clean
    mkdir -p $LOG_DIR
    compile &> $COMPILE_LOG_FILE
    simulate 2>&1 | grep -v "${exclude_0}" \
                  | grep -v "${exclude_1}" \
                  | grep -v "${exclude_2}" \
                  | grep -v "${exclude_3}" > $SIM_LOG_FILE
else
    echo "Nothing ..."
fi

exit 0
