"""
Diff method for OneBa.
"""


import numpy as np

def compare(ref, res):
    """Split `ref` and `res` into real and imaginary parts and compute their
    differences.

    The higher of the real/imag difference is selected as a result.

    Returns the difference as the integer multiplied by 2^-15 - the real
    decimal value of the fixed point notation.
    """

    ref, res = ref.strip(), res.strip()

    ref_re, ref_im = ref[4:], ref[:4]
    res_re, res_im = res[4:], res[:4]

    ref_re = np.int16(int(ref_re, 16))
    ref_im = np.int16(int(ref_im, 16))
    res_re = np.int16(int(res_re, 16))
    res_im = np.int16(int(res_im, 16))

    diff_re = ref_re - res_re
    diff_im = ref_im - res_im

    if (diff_re == 0) and (diff_im == 0):
        equals, diff = True, None
    elif abs(diff_re) > abs(diff_im):
        equals, diff = False, diff_re
    else:
        equals, diff = False, diff_im

    if diff:
        diff = "{:>9.2e}".format(diff * 2**-15)

    return (equals, diff)
