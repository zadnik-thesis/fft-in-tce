--===========================================================================--
-- Processor testbench
--
-- First, instruction and data memory are externally loaded from files. Then,
-- it runs the processor. It keeps track of a current instruction with the
-- exec_count_reg signal.
--
-- It is a modified testbench VHDL template from TTA-Based Codesign Environment:
-- https://github.com/cpc/tce/blob/release-1.16/tce/data/ProGe/tb/testbench.vhdl.tmpl
--===========================================================================--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

use work.testbench_constants.all;
use work.proc_constants.all;
use work.tta0_globals.all;
use work.tta0_imem_mau.all;

entity testbench is
end testbench;

architecture testbench of testbench is

    component clkgen
    generic (
        PERIOD : time);
    port (
        clk : out std_logic;
        en  : in  std_logic := '1');
    end component;

    component proc
    port (
        clk               : in  std_logic;
        rst_x             : in  std_logic;
        mem_wr_lock       : in  std_logic;
        dmem_ext_bit_wr_x : in  std_logic_vector(DMEMDATAWIDTH-1 downto 0);
        dmem_ext_wr_x     : in  std_logic;
        dmem_ext_en_x     : in  std_logic;
        dmem_ext_data     : in  std_logic_vector(DMEMDATAWIDTH-1 downto 0);
        dmem_ext_addr     : in  std_logic_vector(DMEMADDRWIDTH-1 downto 0);
        data_out          : out std_logic_vector(DMEMDATAWIDTH-1 downto 0);
        imem_ext_bit_wr_x : in  std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1
                                                 downto 0);
        imem_ext_wr_x     : in  std_logic;
        imem_ext_en_x     : in  std_logic;
        imem_ext_data     : in  std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1
                                                 downto 0);
        imem_ext_addr     : in  std_logic_vector(IMEMADDRWIDTH-1 downto 0);
        core_busy         : in  std_logic;
        dmem_busy         : out std_logic;
        imem_busy         : out std_logic;
        locked            : out std_logic);
    end component;

    -- Inputs
    signal clk           : std_logic := '1';
    signal rst_x         : std_logic := '0';
    signal mem_wr_lock   : std_logic := '0';
    signal dmem_bit_wr_x : std_logic_vector(DMEMDATAWIDTH-1 downto 0)
                                            := (others => '1');
    signal dmem_wr_x     : std_logic := '1';
    signal dmem_en_x     : std_logic := '1';
    signal dmem_data_in  : std_logic_vector(DMEMDATAWIDTH-1 downto 0)
                                            := (others => '0');
    signal dmem_addr     : std_logic_vector(DMEMADDRWIDTH-1 downto 0)
                                            := (others => '0');
    signal imem_bit_wr_x : std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1
                                            downto 0) := (others => '1');
    signal imem_wr_x     : std_logic := '1';
    signal imem_en_x     : std_logic := '1';
    signal imem_data     : std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1
                                            downto 0) := (others => '0');
    signal imem_addr     : std_logic_vector(IMEMADDRWIDTH-1 downto 0)
                                            := (others => '0');
    -- Outputs
    signal dmem_busy        : std_logic;
    signal imem_busy        : std_logic;
    signal lock_status_wire : std_logic;
    signal data_out         : std_logic_vector(DMEMDATAWIDTH-1 downto 0);

    -- Internal
    signal nexp_sig         : positive  := NEXP;
    signal exec_count_reg   : integer   := 0;
    signal exec_limit       : integer   := -1;
    signal enable_clock     : std_logic := '1';
    signal init_mem_cnt     : integer   := 0;
    signal conflict_cnt     : integer   := 0;

begin -- testbench

    clock : clkgen
        generic map (
            PERIOD => TB_PERIOD)
        port map (
            clk => clk,
            en  => enable_clock);

    dut : proc
        port map (
            clk               => clk,
            rst_x             => rst_x,
            mem_wr_lock       => mem_wr_lock,
            dmem_ext_bit_wr_x => dmem_bit_wr_x,
            dmem_ext_wr_x     => dmem_wr_x,
            dmem_ext_en_x     => dmem_en_x,
            dmem_ext_data     => dmem_data_in,
            dmem_ext_addr     => dmem_addr,
            data_out          => data_out,
            imem_ext_bit_wr_x => imem_bit_wr_x,
            imem_ext_wr_x     => imem_wr_x,
            imem_ext_en_x     => imem_en_x,
            imem_ext_data     => imem_data,
            imem_ext_addr     => imem_addr,
            core_busy         => '0',
            dmem_busy         => dmem_busy,
            imem_busy         => imem_busy,
            locked            => lock_status_wire);

    -- Set the maximum number of instructions to be executed
    exec_counter_limit : process(nexp_sig)
    begin -- exec_counter_limit
        case nexp_sig is
            when 3  => exec_limit <= 35;
            when 4  => exec_limit <= 51;
            when 5  => exec_limit <= 115;
            when 6  => exec_limit <= 211;
            when 7  => exec_limit <= 531;
            when 8  => exec_limit <= 1043;
            when 9  => exec_limit <= 2579;
            when 10 => exec_limit <= 5139;
            when 11 => exec_limit <= 12307;
            when 12 => exec_limit <= 24595;
            when 13 => exec_limit <= 57363;
            when 14 => exec_limit <= 114707;
            when others => exec_limit <= -2;
        end case;
    end process exec_counter_limit;

    -- purpose: Counts executed instructions
    -- type   : sequential
    -- inputs : clk, rst_x
    -- outputs: exec_count_reg
    -- comment: lock_status_wire is 1 cycle behind internal glock
    exec_counter : process (clk, rst_x)
    begin  -- process exec_counter
        if rst_x = '0' then          -- asynchronous reset (active low)
            exec_count_reg <= 0;
        elsif rising_edge(clk) then  -- rising clock edge
            if lock_status_wire = '0' then
                exec_count_reg <= exec_count_reg + 1;
            end if;
        end if;
    end process exec_counter;

    -- Counts memory conflicts (par_mem_logic locking the processor)
    conflicts_counter : process (clk, rst_x)
    begin -- conflicts_counter
        if rst_x = '0' then          -- asynchronous reset (active low)
            conflict_cnt <= 0;
        elsif rising_edge(clk) then  -- rising clock edge
            if not (exec_count_reg = 0) and (lock_status_wire = '1')then
                conflict_cnt <= conflict_cnt + 1;
            end if;
        end if;
    end process conflicts_counter;

    -- Stimulation process
    stim_proc : process
        file f                   : text;
        variable fstatus         : FILE_OPEN_STATUS;
        variable txtline         : line;
        variable v_in_imem       : std_logic_vector(IMEMMAUWIDTH-1 downto 0);
        variable v_in_dmem       : std_logic_vector(DMEMDATAWIDTH-1 downto 0);
        variable v_good          : boolean := false;
        variable FFT_SIZE        : positive;
         -- use true with parallel memory access in par_mem_scheduler
        variable dump_skip_first : boolean := true;
        variable skipped         : boolean := false;
    begin

        FFT_SIZE := 2** NEXP;

        -- Enable init of memories
        -----------------------------------------------------------------------
        wait until rising_edge(clk);
        mem_wr_lock <= '1';

        -- Instruction memory init from .img file
        -----------------------------------------------------------------------
        -- open file
        report "Opening " & IMEM_INIT_FILE & " (read mode) ...";
        file_open(fstatus, f, IMEM_INIT_FILE, read_mode);
        assert fstatus = OPEN_OK report
            "Can't open " & IMEM_INIT_FILE severity error;

        while not endfile(f) loop
            -- read input from file
            readline(f, txtline);
            read(txtline, v_in_imem);
            -- load the inputs
            imem_bit_wr_x <= (others => '0');
            imem_wr_x     <= '0';
            imem_en_x     <= '0';
            imem_data     <= v_in_imem;
            imem_addr     <= std_logic_vector(to_unsigned(init_mem_cnt,
                                                          imem_addr'length));
            init_mem_cnt <= init_mem_cnt + 1;
            wait until rising_edge(clk);
        end loop;
        -- close file
        report "Closing " & IMEM_INIT_FILE & " ...";
        file_close(f);
        -- restore to defaults
        imem_bit_wr_x <= (others => '1');
        imem_wr_x     <= '1';
        imem_en_x     <= '1';
        imem_data     <= (others => '0');
        imem_addr     <= (others => '0');
        init_mem_cnt <= 0;
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);

        -- Data memory init from .img file
        -----------------------------------------------------------------------
        -- open file
        report "Opening " & DMEM_INIT_FILE & " (read mode) ...";
        file_open(fstatus, f, DMEM_INIT_FILE, read_mode);
        assert fstatus = OPEN_OK report
            "Can't open " & DMEM_INIT_FILE severity error;

        while not endfile(f) loop
            -- read input from file
            readline(f, txtline);
            read(txtline, v_in_dmem);
            -- load the inputs
            dmem_bit_wr_x <= (others => '0');
            dmem_wr_x     <= '0';
            dmem_en_x     <= '0';
            dmem_data_in  <= v_in_dmem;
            dmem_addr     <= std_logic_vector(to_unsigned(init_mem_cnt,
                                                          dmem_addr'length));
            init_mem_cnt <= init_mem_cnt + 1;
            wait until rising_edge(clk);
        end loop;
        -- close file
        report "Closing " & DMEM_INIT_FILE & " ...";
        file_close(f);
        -- restore to defaults
        dmem_bit_wr_x <= (others => '1');
        dmem_wr_x     <= '1';
        dmem_en_x     <= '1';
        dmem_data_in  <= (others => '0');
        dmem_addr     <= (others => '0');
        init_mem_cnt  <= 0;

        -- Finish loading memories
        -----------------------------------------------------------------------
        mem_wr_lock <= '0';

        -- Report executions
        -----------------------------------------------------------------------
        if exec_limit < 0 then
            report "Run clock indefinitely.";
        else
            report "Execution limit is " & integer'image(exec_limit) &
                   " instructions.";
        end if;

        -- Start processing
        -----------------------------------------------------------------------
        wait until rising_edge(clk);
        rst_x <= '1';

        if exec_limit < 0 then
            wait;
        else
            wait on exec_count_reg until exec_count_reg >= exec_limit;
        end if;

        -- Dump number of conflicts
        -----------------------------------------------------------------------
        -- open file
        report "Opening " & CONFLICTS_FILE & " (write mode) ...";
        file_open(fstatus, f, CONFLICTS_FILE , write_mode);
        assert fstatus = OPEN_OK report "Can't open " & CONFLICTS_FILE severity error;
        -- write to file
        -- write(txtline, integer'image(conflict_cnt));
        write(txtline, conflict_cnt);
        writeline(f, txtline);
        -- close file
        report "Closing " & CONFLICTS_FILE & " ...";
        file_close(f);

        -- Stop processing
        -----------------------------------------------------------------------
        rst_x         <= '0';
        wait until rising_edge(clk);

        -- Dump data memory
        -----------------------------------------------------------------------
        -- open file
        report "Opening " & OUTFILE & " (write mode) ...";
        file_open(fstatus, f, OUTFILE , write_mode);
        assert fstatus = OPEN_OK report "Can't open " & OUTFILE severity error;
        -- write memory to file
        mem_wr_lock   <= '1';
        while init_mem_cnt < FFT_SIZE loop
            dmem_en_x    <= '0';
            dmem_addr    <= std_logic_vector(to_unsigned(init_mem_cnt,
                                                          dmem_addr'length));
            init_mem_cnt <= init_mem_cnt + 1;
            wait until rising_edge(clk);
            wait for 10 ps;
            if dump_skip_first = false then
                hwrite(txtline, data_out, right, 8);
                writeline(f, txtline);
            else
                skipped := true;
                dump_skip_first := false;
            end if;
        end loop;
        -- Write the last sample if the first was skipped
        if skipped = true then
            dmem_en_x    <= '0';
            dmem_addr    <= std_logic_vector(to_unsigned(FFT_SIZE-1,
                                                          dmem_addr'length));
            wait until rising_edge(clk);
            wait for 10 ps;
                hwrite(txtline, data_out, right, 8);
                writeline(f, txtline);
        end if;
        dmem_en_x     <= '1';
        dmem_addr     <= (others => '0');
        mem_wr_lock   <= '0';
        -- close file
        report "Closing " & OUTFILE & " ...";
        file_close(f);

        -- End the testbench
        -----------------------------------------------------------------------
        wait until rising_edge(clk);
        enable_clock  <= '0';
        assert false report "Simulation finished fine" severity failure;

    end process stim_proc;

end testbench;
