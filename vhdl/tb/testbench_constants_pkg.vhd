--===========================================================================--
-- Package containing constants related to testbench
--
-- Specifies data/instruction memory init files. These files contain contents
-- of the memories, one word per line.
--===========================================================================--

use work.proc_constants.all;

package testbench_constants is

    -- Testbench period (default is 250MHz)
    constant TB_PERIOD : time := 4 ns;

    -- Input files
    constant DMEM_INIT_FILE : string :=
        "img/dmem_iodata_init_" & integer'image(NEXP) & ".img";
    constant IMEM_INIT_FILE : string :=
        "img/imem_init_" & integer'image(NEXP) & ".img";

    -- Output files
    constant OUTFILE : string :=
        "res/ParMem_CactiBlocks/out_" & integer'image(NEXP);
    constant CONFLICTS_FILE : string :=
        "res/conflicts/conflicts_" & integer'image(NEXP) & ".txt";

end testbench_constants;
