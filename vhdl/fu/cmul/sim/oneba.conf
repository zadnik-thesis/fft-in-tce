# OneBa configuration file
# https://gitlab.com/kubouch/oneba

# Paths can be:
#    * relative to the oneba root directory (where config.txt is)
#    * absolute
# Can't use '$' (i.e. in rep_lines for shell scripts) ('%', '&')
# Option names are turned to lowercase (not sections)

[DEFAULT]
# Don't put anything here

# Diff, temp and backup directories are created if not present
# Simulation and reference not created
[Dirs]
cmul       = ..
reference  = ${cmul}/sim/ref
simulation = ${cmul}/sim/res
diff       = ${cmul}/sim/diff
backup     = ${cmul}/sim/tmp
temp       = ${cmul}/sim/tmp

# ref/sim filenames get compared if their filenames match after the prefix
[Prefixes]
ref_files  = ref
sim_files  = res
diff_files =

# Order matters
[Variables]
resfile = res/res.txt

# Compilation script
[Com_script]
file_path = ${Dirs:cmul}/sim/ghdl_compile.sh
exec_with = sh
args      =

# Simulation script
[Sim_script]
file_path = ${Com_script:file_path}
exec_with = sh
args      = sim

# Clean script (executed when --clean flag is passed to the program)
[Clean_script]
file_path = ${Com_script:file_path}
exec_with = sh
args      = clean

# Patch operations
# Each [Patch_*] section corresponds to one changed line.
# For changing more lines of one file, more patches need to be created.
# Use line number starting from 1 (as in most editors).
# Variables are placed instead of '{}' in the order they are written.
# \n is added in the end of each replaced line automatically.
[Patch_tb]
file_path    = ${Dirs:cmul}/tb/tb_cmul_arith.vhd
rep_line_num = 66
rep_line     = \t\tvariable outfile  : string(1 to 11) := "{}";
variables    = resfile
search_str   = variable outfile
