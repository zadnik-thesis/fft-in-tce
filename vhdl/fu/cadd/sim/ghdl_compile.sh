#!/bin/sh

####
# Variables
####

SCRIPTDIR=$(dirname "$0")
cd $SCRIPTDIR
WORKDIR="../work"

GHDLFLAGS="--workdir=$WORKDIR --std=08"
SIMFLAGS="--assert-level=none"

FILES="../../shared/constants.vhd"
FILES+=" ../../shared/tb_utils.vhd"
FILES+=" ../cadd.vhd"
FILES+=" ../fu_cadd_always_1.vhd"
FILES+=" ../tb/tb_cadd_always_1.vhd"

ENTITY="tb_cadd_always_1"
OUTDIR="res"
OUTFILE="$OUTDIR/res.txt"
LOGFILE="log.txt"

####
# Functions
####

clean()
{
    echo "Cleaning ..."
    rm -rf $WORKDIR
    rm -rf $ENTITY
    rm -rf e~$ENTITY.o
    rm -rf $LOGFILE
}

compile()
{
    mkdir -p $WORKDIR
    echo "Compiling..."
    if [ $# -eq 0 ]; then
        ghdl -s $GHDLFLAGS $FILES
        ghdl -a $GHDLFLAGS $FILES
        ghdl -e $GHDLFLAGS $ENTITY
    fi
}

simulate()
{
    mkdir -p $OUTDIR
    echo "Simulating ... (log in $LOGFILE)"
    if [ -e $ENTITY ]; then
        ./$ENTITY &> $LOGFILE
    else
        # Some GHDL versions do not produce binary.
        ghdl -r $GHDLFLAGS $ENTITY $SIMFLAGS &> $LOGFILE
    fi
}

####
# Main script
####

if [ "$1" = "sim" ]; then
    simulate
elif [ "$1" = "clean" ]; then
    clean
elif [ $# -eq 0 ]; then
    clean
    compile
else
    echo "Nothing ..."
fi
