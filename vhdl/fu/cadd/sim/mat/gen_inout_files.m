% Generate input and reference files for complex adder testbench.
% Values of inputs lie in the interval [-(1-2^(-15)), 1-2^(-15)].
%
% Number format: 32 bit fixed point complex numbers
%    - bits 1..16 real part (Q1.15)
%    - bits 17..32 imaginary part (Q1.15)
%
% Line format (input):
%     r_xxxxXXXX ('r' - 1 for radix-2, 0 for radix-4,
%                 uppercase - real, lowercase - imag,
%                 '_' - space)

close all
clear all
clc

infile = '../in.txt';
resfile = '../ref/ref.txt';

% Number of values to test
nvalues = 1000;

% The maximum value since 1 is out of the allowed range.
% Equals to 1 - 2^(-15) where 2^(-15) is the quantization step.
maxval = 0.999969482421875;

% Get the input vector
try
    load('inp.mat');    
    fprintf('Input values loaded from a file.\n')
catch
    % First are combinations of (0, 1, -1) for real/imag parts
    re_im_values = [0, maxval, -maxval];
    combs  = zeros(9,1);
    n = 1;
    
    for xi = re_im_values
        for xr = re_im_values                
            combs(n) = xr + xi*1j;            
            n = n+1;
        end
    end
    
    inp_combs = nchoosek(combs, 4);
    inp_ones = zeros(length(inp_combs)*4*2, 1);
    
    for n = 1:length(inp_combs)        
        inp_ones(n*8-7:n*8) = repmat(inp_combs(n,:)', 2, 1);
    end
    
    % Random values
    inp_rands = zeros(125*8, 1);
    for n = 1:125
        four_rands = rand(4,1)*2-1 + (rand(4,1)*2-1) * 1j;
        inp_rands(n*8-7:n*8) = repmat(four_rands, 2, 1);
    end
    
    % Final input vector
    inp = [ inp_ones ; zeros(8,1) ; inp_rands ];
    inp = [ inp ; zeros(4,1) ];    
    fprintf('Input values loaded from a file.\n')
end

% Radix-2 flags
rx2 = repmat( [ zeros(4,1) ; ones(4,1) ], length(inp)/8, 1);
rx2 = [ rx2 ; zeros(4,1) ];

% Four last loads for CADD
inp = [ inp ; zeros(4,1) ];
% Fixed point inputs
fp_in = fi(inp, true, 16, 15);

% Compute results
fp_res = fi(zeros(length(fp_in)-4,1), true, 16, 15, 'RoundingMethod', 'Floor');
for n = 1:4:length(fp_res)
    fp_P = fp_in(n:n+3);
    rx2_flag = rx2(n);
    Y = butterfly(fp_P, rx2_flag);
    fp_res(n:n+3) = Y;
end
fp_res = [ zeros(4,1) ; fp_res ];

% Print input file
in_fid = fopen(infile, 'wt');
for n = 1:length(fp_in)
    s_in = sprintf('%s%s', hex(imag(fp_in(n))), hex(real(fp_in(n))));    
    fprintf(in_fid, '%d %s\n', rx2(n), upper(s_in));   
end
fclose(in_fid);

% Print output reference file
res_fid = fopen(resfile, 'wt');
for n = 1:length(fp_res)
    s_res = sprintf('%s%s', hex(imag(fp_res(n))), hex(real(fp_res(n))));    
    fprintf(res_fid, '%s\n', upper(s_res));   
end
fclose(res_fid);
