--===========================================================================--
-- Complex adder wrapper for TCE (latency 1)
--
-- Latency 1 is just theoretical. This FU has an internal register and its own
-- loading system. Real latency should be probably considered 4 (see cadd.vhd).
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- It is a top level entity for cadd_full providing a standard TCE-compatible
-- interface
--===========================================================================--


--===========================================================================--
-- Entity of CADD latency 1
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;

entity fu_cadd_always_1 is
    generic (
        dataw : integer := DATA_W;
        halfw : integer := HALF_W;
        busw  : integer := BUS_W);
    port (
        t1data   : in  std_logic_vector(dataw-1 downto 0);
        t1load   : in  std_logic;
        o1data   : in  std_logic_vector(0 downto 0);  -- can't be std_logic
        o1load   : in  std_logic;
        r1data   : out std_logic_vector(busw-1 downto 0);
        glock    : in  std_logic;
        rstx     : in  std_logic;
        clk      : in  std_logic);
end fu_cadd_always_1;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of CADD latency 1
-------------------------------------------------------------------------------
architecture rtl of fu_cadd_always_1 is

    component cadd_full
        generic (
            dataw  : integer;
            halfw  : integer);
        port (
            t_i    : in  std_logic_vector (dataw-1 downto 0);
            load_i : in  std_logic;
            rx2_i  : in  std_logic;
            res_o  : out std_logic_vector (dataw-1 downto 0);
            glock  : in std_logic;
            rstx   : in std_logic;
            clk    : in std_logic);
    end component;

    signal s_o1data      : std_logic;
    signal t1reg         : std_logic_vector (dataw-1 downto 0);
    signal o1reg         : std_logic;
    signal o1tmp         : std_logic;
    signal control       : std_logic_vector (1 downto 0);
    signal r1            : std_logic_vector (dataw-1 downto 0);

begin -- rtl

    s_o1data <= o1data(0);

    fu_arch : cadd_full
        generic map (
            dataw => dataw,
            halfw => halfw)
        port map (
            t_i    => t1data,
            load_i => t1load,
            rx2_i  => s_o1data,
            res_o  => r1,
            glock  => glock,
            rstx   => rstx,
            clk    => clk);

    control <= o1load & t1load;

    regs : process (clk, rstx)
    begin
        if rstx = '0' then
            t1reg <= (others => '0');
            o1reg <= '0';
            o1tmp <= '0';
        elsif rising_edge(clk) then
            if glock = '0' then
                case control is
                    when "01" =>
                        o1reg <= o1tmp;
                        t1reg <= t1data;
                    when "10" =>
                        o1tmp <= s_o1data;
                    when "11" =>
                        o1reg <= s_o1data;
                        o1tmp <= s_o1data;
                        t1reg <= t1data;
                    when others => null;
                end case;
            end if;
        end if;
    end process regs;

    r1data <= std_logic_vector(resize(signed(r1), busw));

end rtl;
--===========================================================================--
