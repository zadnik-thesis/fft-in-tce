library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tf_lut is
    generic (
        dataw     : natural;
        lut_addrw : natural);
    port (
        addr_i    : in  std_logic_vector(lut_addrw-1 downto 0);
        tf_o      : out std_logic_vector(dataw    -1 downto 0));
end tf_lut;

architecture Structural of tf_lut is

    -- component bram_2049x32
    --     generic (
    --         dataw     : natural;
    --         lut_addrw : natural);
    --     port (
    --         addr_i    : in  std_logic_vector(lut_addrw-1 downto 0);
    --         tf_o      : out std_logic_vector(dataw    -1 downto 0);
    --         clk       : in  std_logic);
    -- end component;

    component distr_2049x32
        generic (
            dataw     : natural;
            lut_addrw : natural);
        port (
            addr_i    : in  std_logic_vector(lut_addrw-1 downto 0);
            tf_o      : out std_logic_vector(dataw    -1 downto 0));
    end component;

begin -- Structural

    lut : distr_2049x32
        generic map (
            dataw     => dataw,
            lut_addrw => lut_addrw)
        port map (
            addr_i    => addr_i,
            tf_o      => tf_o);

end Structural;
