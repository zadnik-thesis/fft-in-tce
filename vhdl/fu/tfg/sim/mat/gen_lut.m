% Generate twiddle factor LUT values for tfg.

close all
clear all
clc

lut_file = '../../tf_lut/lut_from_matlab';

N = 2^14;
M = N/8 + 1;
tf = zeros(M,1);
for n = 1:M
    tf(n) = exp(-1j * 2 * pi * (n-1) / N);
end

fp_tf = fi(tf, true, 16, 15);

fid = fopen(lut_file, 'wt');
for n = 1:length(fp_tf)
    hexnum = sprintf('%s%s', hex(imag(fp_tf(n))), hex(real(fp_tf(n))));
    if n < length(fp_tf)
        fprintf(fid, '%-4d => x"%s",\n', n-1, hexnum);
    else
        fprintf(fid, '%-4d => x"%s"\n', n-1, hexnum);
    end
end