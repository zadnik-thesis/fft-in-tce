-- Weight for k (twiddle factor index)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity weight is
    generic (
        idxw      : natural;
        stagew    : natural;
        nexpw     : natural;
        lut_addrw : natural);
    port (
        idx_i     : in  std_logic_vector(idxw  -1 downto 0);
        stage_i   : in  std_logic_vector(stagew-1 downto 0);
        nexp_i    : in  std_logic_vector(nexpw -1 downto 0);
        w_o       : out std_logic_vector(idxw  -1 downto 0));
end weight;

architecture Behavioral of weight is

    signal idx   : unsigned(idx_i'range);
    signal stage : unsigned(stage_i'range);
    signal nexp  : unsigned(nexp_i'range);
    signal w     : unsigned(w_o'range);

begin -- Behavioral

    idx   <= unsigned(idx_i);
    stage <= unsigned(stage_i);
    nexp  <= unsigned(nexp_i);

    process(idx, stage, nexp)
        variable stage_sc : unsigned(stage'high+1 downto 0);
        variable shift    : integer;
        variable idx_sc   : unsigned(idx'range);
        variable i        : natural;
        variable i_xor    : natural;
        variable nbits    : natural;  -- how many bits to rotate
        variable w_tmp    : unsigned(lut_addrw-1 downto 0);
    begin
        if to_integer(stage) = 0 then
            w_tmp := (others => '0');
            w     <= (others => '0');
        else
            stage_sc := resize(stage, stage_sc'length);
            shift    := to_integer(nexp) - to_integer(shift_left(stage_sc, 1));
            idx_sc   := shift_right(idx, shift);
            nbits    := to_integer(shift_left(stage_sc, 1));
            -- bit positions are (i xor '1'), backwards
            for i in 0 to w_tmp'length-1 loop
                i_xor := to_integer(to_unsigned(i, nexpw) xor
                                    to_unsigned(1, nexpw));
                w_tmp(w_tmp'left-i) := idx_sc(i_xor);
            end loop;
            w <= resize(shift_right(w_tmp, w_tmp'length-nbits), w'length);
        end if;
    end process;

    w_o <= std_logic_vector(w);

end Behavioral;
