--==========================================================================--
-- Testbench for fu_tfg_always_6
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- standard: VHDL-2008
--==========================================================================--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
-- reding/writing std_logic(_vector) from/to files:
--use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

library work;
use work.constants.all;
use work.tb_utils.all;

entity tb_tfg_always_6 is
    -- port ( );
end tb_tfg_always_6;

architecture Behavioral of tb_tfg_always_6 is

    -- uut
    component fu_tfg_always_6
        generic (
            dataw  : positive := DATA_W;
            busw   : positive := BUS_W;
            nexpw  : positive := NEXP_W);
        port (
            t1data : in  std_logic_vector(dataw-1 downto 0);
            t1load : in  std_logic;
            o1data : in  std_logic_vector(nexpw-1 downto 0);
            o1load : in  std_logic;
            r1data : out std_logic_vector(busw -1 downto 0);
            r2data : out std_logic;
            glock  : in  std_logic;
            rstx   : in  std_logic;
            clk    : in  std_logic);
    end component;

    -- inputs
    signal lidx         : std_logic_vector(31 downto 0) := (others => '0');
    signal nexp         : std_logic_vector(3 downto 0)  := (others => '0');
    -- loads
    signal lidx_load    : std_logic := '0';
    signal nexp_load    : std_logic := '0';
    -- outputs
    signal tf           : std_logic_vector(31 downto 0) := (others => '0');
    signal rx2          : std_logic := '0';
    -- clock period
    signal clk          : std_logic := '0';
    constant clk_period : time      := 10 ns;
    -- control
    signal glock        : std_logic := '1';
    signal rstx         : std_logic := '0';
    -- constants
    constant C_NEXP     : positive  := 3;
    constant C_LATENCY  : natural   := 6;

begin

    uut : fu_tfg_always_6
        generic map (
            dataw  => DATA_W,
            busw   => BUS_W,
            nexpw  => NEXP_W)
        port map (
            t1data => lidx,
            t1load => lidx_load,
            o1data => nexp,
            o1load => nexp_load,
            r1data => tf,
            r2data => rx2,
            glock  => glock,
            rstx   => rstx,
            clk    => clk);

    -- clock signal
    clk <= not clk after clk_period/2;

    -- stimulus process
    stim_proc : process
        file f_tf       : text;
        file f_rx2       : text;
        variable fstatus : FILE_OPEN_STATUS;
        variable tffile  : string(1 to 6) := "tf.txt";
        variable rx2file : string(1 to 7) := "rx2.txt";
        variable outline : line;
        variable cnt     : integer := 0;
        variable offset  : integer := C_LATENCY;
        variable repeats : integer := ((C_NEXP+1)/2)*(2**C_NEXP) + C_LATENCY;
    begin

        -- init
        wait_until_rising_edges(clk, 2);
        rstx <= '1';
        wait until rising_edge(clk);
        glock <= '0';
        wait until rising_edge(clk);
        nexp_load <= '1';
        nexp <= std_logic_vector(to_unsigned(C_NEXP, nexp'length));
        wait until rising_edge(clk);
        nexp_load <= '0';

        -- open files
        report "Opening " & tffile & " ...";
        file_open(fstatus, f_tf, tffile, write_mode);
        assert fstatus = OPEN_OK report "File " & tffile & " does not exist."
                                          severity error;
        report "Opening " & rx2file & " ...";
        file_open(fstatus, f_rx2, rx2file, write_mode);
        assert fstatus = OPEN_OK report "File " & rx2file & " does not exist."
                                          severity error;

        -- sim loop
        report "Simulation loop. " & integer'image(repeats) & " rounds.";
        for cnt in 0 to repeats-1 loop
            -- load new values
            lidx_load <= '1';
            lidx <= std_logic_vector(to_unsigned(cnt, lidx'length));
            -- wait 1 clock cycle
            wait until rising_edge(clk);
            -- write values to file
             hwrite(outline, tf, right, 8);
            writeline(f_tf, outline);
             write(outline, rx2, left, 1);
            writeline(f_rx2, outline);
        end loop;
        lidx_load <= '0';

        report "Closing " & tffile & " ...";
        file_close(f_tf);
        report "Closing " & rx2file & " ...";
        file_close(f_rx2);

        wait until rising_edge(clk);

        report "Simulation END";
        -- VHDL-2008
        std.env.finish(0);

    end process stim_proc;

end Behavioral;
