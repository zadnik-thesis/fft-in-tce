library ieee;
use ieee.std_logic_1164.all;

package tb_utils is

    -- Wait until n rising edges of clk. Used to avoid race conditions in tb.
    -- Adopted from apalopohapa on Stack Exchange:
    -- https://electronics.stackexchange.com/a/148618/164174
    procedure wait_until_rising_edges(signal clk : in std_logic;
                                               n : in natural);

end package tb_utils;

package body tb_utils is

    procedure wait_until_rising_edges(signal clk : in std_logic;
                                               n : in natural) is
    begin
        for i in 1 to n loop
            wait until rising_edge(clk);
        end loop;
    end procedure;

end package body tb_utils;
