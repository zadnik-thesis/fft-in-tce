-- Split linear index into 'stage' and 'idx'
-- First 'nexp' LSB bits are idx, the rest is stage

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity idx_stage_split is
    generic (
        dataw  : positive;
        nexpw  : positive;
        stagew : positive;
        idxw   : positive);
    port (
        lidx_i  : in  std_logic_vector (dataw-1 downto 0);
        nexp_i  : in  std_logic_vector (nexpw-1 downto 0);
        idx_o   : out std_logic_vector (idxw-1 downto 0);
        stage_o : out std_logic_vector (stagew-1 downto 0));
end idx_stage_split;

architecture Behavioral of idx_stage_split is

    signal lidx  : unsigned(dataw-1 downto 0);
    signal nexp  : unsigned(nexpw-1 downto 0);
    signal idx   : unsigned(dataw-1 downto 0);
    signal stage : unsigned(dataw-1 downto 0);

begin -- Behavioral

    lidx <= unsigned(lidx_i);
    nexp <= unsigned(nexp_i);

    split : process (nexp, lidx)
        variable i : natural;
    begin
        for i in 0 to dataw-1 loop
            if i < to_integer(nexp) then
                idx(i)   <= lidx(i);
                stage(i) <= lidx(i + to_integer(nexp));
            else
                idx(i)   <= '0';
                stage(i) <= '0';
            end if;
        end loop;
    end process split;

    idx_o   <= std_logic_vector(resize(idx, idx_o'length));
    stage_o <= std_logic_vector(resize(stage, stage_o'length));

end Behavioral;
