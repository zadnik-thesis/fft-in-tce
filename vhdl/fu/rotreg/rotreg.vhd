--===========================================================================--
-- Rotating register (as a delay block)
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- Delays an input number and outputs it several (configurable) cycles later
-- Delay is controlled by reg_count generic and must be higher than 1.
--===========================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rotreg is
    generic (
        dataw     : positive;
        reg_count : positive); -- delay: must be more than 1
    port (
        d_i       : in  std_logic_vector(dataw-1 downto 0);
        load_i    : in  std_logic;
        q_o       : out std_logic_vector(dataw-1 downto 0);
        clk       : in  std_logic;
        glock     : in  std_logic;
        rstx      : in  std_logic);
end rotreg;

architecture Behavioral of rotreg is

    type reg_t is
        array (natural range <>) of std_logic_vector(dataw-1 downto 0);

    signal reg    : reg_t(reg_count-1 downto 0);
    signal set    : std_logic_vector(reg_count-1 downto 0);
    signal cnt_wr : natural;
    signal cnt_rd : natural;

begin

    counters : process(clk, rstx)
    begin -- counters
        if rstx = '0' then
            cnt_rd <= 1;
            cnt_wr <= 0;
        elsif rising_edge(clk) then
            if (glock = '0') then
                -- read address
                if cnt_rd < reg_count-1 then
                    cnt_rd <= cnt_rd + 1;
                else
                    cnt_rd <= 0;
                end if;
                -- write address (one cycle behind read)
                if cnt_rd = reg_count-1 then
                    cnt_wr <= reg_count-1;
                else
                    cnt_wr <= cnt_rd;
                end if;
            end if;
        end if;
    end process counters;

    read_write: process(clk, rstx)
    begin -- read_write
        if rstx = '0' then
            for i in 0 to (reg'length-1) loop
                reg(i) <= (others => '0');
            end loop;
            set <= (others => '0');
        elsif rising_edge(clk) then
            if glock = '0' then
                -- write
                if load_i = '1' then
                    reg(cnt_wr) <= d_i;
                    set(cnt_wr) <= '1';
                -- remove entry when not loading values
                elsif set(cnt_rd) = '1' then
                    reg(cnt_rd) <= (others => '0');
                    set(cnt_rd) <= '0';
                end if;
                -- read
                q_o <= reg(cnt_rd);
            end if;
        end if;
    end process read_write;

end Behavioral;
