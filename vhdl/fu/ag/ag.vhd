--===========================================================================--
-- Address generator
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- Generates input/output addresses for an in-place computation of a mixed
-- radix-4/2 FFT.
--===========================================================================--


--===========================================================================--
-- Entity of a pure combinatory AG
-------------------------------------------------------------------------------
-- Generics:
--   dataw     : Width of the data bus (and linear index input)
--   nexpw     : Width of nexp signals
--   idxw      : Width of separated index signal
--   stagew    : Width of separated stage signal
--   mau_shift : How much to shift the address left to compensate for different
--               data and MAU (minimum adressable unit) size

-- Inputs:
--   lidx_i : Linear counter from an adder (add+1)
--   nexp_i : Current FFT size as an exponent (N = 2**nexp)
--   base_i : A fixed address offset (final_addr = addr + base)

-- Outputs:
--   addr_o : Final computed address
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ag_arith is
    generic (
        dataw     : positive;
        nexpw     : positive;
        idxw      : positive;
        stagew    : positive;
        mau_shift : natural);
    port (
        lidx_i    : in  std_logic_vector(dataw-1 downto 0);
        nexp_i    : in  std_logic_vector(nexpw-1 downto 0);
        base_i    : in  std_logic_vector(dataw-1 downto 0);
        addr_o    : out std_logic_vector(dataw-1 downto 0));
end ag_arith;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of a pure combinatory AG
-------------------------------------------------------------------------------
architecture Behavioral of ag_arith is

    component idx_stage_split
        generic (
            dataw   : positive;
            nexpw   : positive;
            idxw    : positive;
            stagew  : positive);
        port (
            lidx_i  : in  std_logic_vector(dataw-1 downto 0);
            nexp_i  : in  std_logic_vector(nexpw-1 downto 0);
            idx_o   : out std_logic_vector(idxw-1 downto 0);
            stage_o : out std_logic_vector(stagew-1 downto 0));
    end component;

    -- connecting signals
    signal s_idx      : std_logic_vector(idxw-1 downto 0);
    signal s_stage    : std_logic_vector(stagew-1 downto 0);
    -- inputs
    signal nexp       : unsigned(nexp_i'range);
    signal base_addr  : unsigned(base_i'range);
    -- computation stage info
    signal stage      : unsigned(s_stage'range);
    signal last_stage : unsigned(s_stage'range);
    -- lin_idx without stage
    signal idx        : unsigned(s_idx'range);
    -- output
    signal addr_out   : unsigned(idx'range);

begin -- Behavioral

    nexp      <= unsigned(nexp_i);
    base_addr <= unsigned(base_i);

    -- Extract the stage and current index (idx) within the stage from lin_idx
    u0 : idx_stage_split
        generic map (
            dataw   => dataw,
            nexpw   => nexpw,
            idxw    => idxw,
            stagew  => stagew)
        port map (
            lidx_i  => lidx_i,
            nexp_i  => nexp_i,
            idx_o   => s_idx,
            stage_o => s_stage);

    idx   <= unsigned(s_idx);
    stage <= unsigned(s_stage);

    -- Last stage
    last_stage <= resize(shift_right(nexp-1, 1), last_stage'length);

    -- Address generation
    permutation : process (nexp, stage, last_stage, idx)
        --variable pos   : natural
        variable s     : unsigned(idx'range);
        variable pos   : unsigned(idx'range);
        variable lsbs  : unsigned(idx'range);
        variable left  : unsigned(idx'range);
        variable right : unsigned(idx'range);
        variable addr  : unsigned(idx'range);
    begin
        if stage = last_stage then
            s     := (others => '0');
            pos   := (others => '0');
            lsbs  := (others => '0');
            left  := (others => '0');
            right := (others => '0');
            -- Addresses in the last stage are in normal order
            addr_out <= shift_left(idx, mau_shift);
        else
            s     := resize(stage, s'length);
            -- Insert the last 2 LSBs in the 'pos'th bit position
            pos   := nexp - shift_left(s, 1);
            lsbs(lsbs'high downto 2) := (others => '0');
            lsbs(1 downto 0) := idx(1 downto 0);
            left  := shift_left(shift_right(idx, to_integer(pos)),
                                to_integer(pos));
            right := shift_right(idx-left-lsbs, 2);
            addr  := left or shift_left(lsbs, to_integer(pos-2)) or right;
            addr_out <= shift_left(addr, mau_shift);  -- MAU compensation
        end if;
    end process permutation;

    addr_o <= std_logic_vector(resize(addr_out, addr_o'length) + base_addr);

end Behavioral;
--===========================================================================--


--===========================================================================--
-- Entity of a pipelined AG with 1 stage
-------------------------------------------------------------------------------
-- Generics:
--   dataw  : Width of the data bus (and linear index input)
--   nexpw  : Width of nexp signals
--   idxw   : Width of separated index signal
--   stagew : Width of separated stage signal
--   mau_shift : How much to shift the address left to compensate for different
--               data and MAU (minimum adressable unit) size
--
-- Inputs:
--   lidx_i : Linear counter from an adder (add+1)
--   nexp_i : Current FFT size as an exponent (N = 2**nexp)
--   base_i : A fixed address offset (final_addr = addr + base)
--   glock  : Global lock
--   rstx   : Asynchdonous reset (active low)
--   clk    : Clock
--
-- Outputs:
--   addr_o : Final computed address
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ag_pipe_1 is
    generic (
        dataw     : positive;
        nexpw     : positive;
        idxw      : positive;
        stagew    : positive;
        mau_shift : natural);
    port (
        lidx_i    : in  std_logic_vector(dataw-1 downto 0);
        nexp_i    : in  std_logic_vector(nexpw-1 downto 0);
        base_i    : in  std_logic_vector(dataw-1 downto 0);
        addr_o    : out std_logic_vector(dataw-1 downto 0);
        glock     : in std_logic;
        rstx      : in std_logic;
        clk       : in std_logic);
end ag_pipe_1;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of a pipelined AG with 1 stage
-------------------------------------------------------------------------------
architecture Behavioral of ag_pipe_1 is

    component idx_stage_split
        generic (
            dataw   : positive;
            nexpw   : positive;
            idxw    : positive;
            stagew  : positive);
        port (
            lidx_i  : in  std_logic_vector(dataw-1 downto 0);
            nexp_i  : in  std_logic_vector(nexpw-1 downto 0);
            idx_o   : out std_logic_vector(idxw-1 downto 0);
            stage_o : out std_logic_vector(stagew-1 downto 0));
    end component;

    -- connecting signals
    signal s_idx       : std_logic_vector(idxw-1 downto 0);
    signal s_stage     : std_logic_vector(stagew-1 downto 0);
    -- inputs
    signal nexp        : unsigned(nexp_i'range);
    signal base_addr   : unsigned(base_i'range);
    -- computation stage info
    signal stage       : unsigned(s_stage'range);
    signal last_stage  : unsigned(s_stage'range);
    -- lin_idx without stage
    signal idx         : unsigned(s_idx'range);
    -- output
    signal addr_out    : unsigned(idx'range);
    -- register
    signal s_idx_reg   : std_logic_vector(s_idx'range);
    signal s_stage_reg : std_logic_vector(s_stage'range);

begin -- Behavioral

    nexp      <= unsigned(nexp_i);
    base_addr <= unsigned(base_i);

    -- Extract the stage and current index (idx) within the stage from lin_idx
    u0 : idx_stage_split
        generic map (
            dataw   => dataw,
            nexpw   => nexpw,
            idxw    => idxw,
            stagew  => stagew)
        port map (
            lidx_i  => lidx_i,
            nexp_i  => nexp_i,
            idx_o   => s_idx,
            stage_o => s_stage);

    idx   <= unsigned(s_idx_reg);
    stage <= unsigned(s_stage_reg);

    -- Last stage
    last_stage <= resize(shift_right(nexp-1, 1), last_stage'length);

    -- Address generation
    permutation : process (nexp, stage, last_stage, idx)
        --variable pos   : natural
        variable s     : unsigned(idx'range);
        variable pos   : unsigned(idx'range);
        variable lsbs  : unsigned(idx'range);
        variable left  : unsigned(idx'range);
        variable right : unsigned(idx'range);
        variable addr  : unsigned(idx'range);
    begin
        if stage = last_stage then
            s     := (others => '0');
            pos   := (others => '0');
            lsbs  := (others => '0');
            left  := (others => '0');
            right := (others => '0');
            -- Addresses in the last stage are in normal order
            addr_out <= shift_left(idx, mau_shift);
        else
            s     := resize(stage, s'length);
            -- Insert the last 2 LSBs in the 'pos'th bit position
            pos   := nexp - shift_left(s, 1);
            lsbs(lsbs'high downto 2) := (others => '0');
            lsbs(1 downto 0) := idx(1 downto 0);
            left  := shift_left(shift_right(idx, to_integer(pos)),
                                to_integer(pos));
            right := shift_right(idx-left-lsbs, 2);
            addr  := left or shift_left(lsbs, to_integer(pos-2)) or right;
            addr_out <= shift_left(addr, mau_shift);  -- MAU compensation
        end if;
    end process permutation;

    reg : process(clk, rstx)
    begin -- reg
        if rstx = '0' then
            s_idx_reg   <= (others => '0');
            s_stage_reg <= (others => '0');
        end if;
        if rising_edge(clk) then
            if glock = '0' then
                s_idx_reg   <= s_idx;
                s_stage_reg <= s_stage;
            end if;
        end if;
    end process reg;

    addr_o <= std_logic_vector(resize(addr_out, addr_o'length) + base_addr);

end Behavioral;
--===========================================================================--
