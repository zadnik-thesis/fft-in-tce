#!/bin/sh

####
# Variables
####

SCRIPTDIR=$(dirname "$0")
cd $SCRIPTDIR
WORKDIR="../work"

GHDLFLAGS="--workdir=$WORKDIR --std=08"
SIMFLAGS="--assert-level=none"

FILES="../../shared/constants.vhd ../../shared/idx_stage_split.vhd ../ag.vhd ../tb/tb_ag_arith.vhd"

ENTITY="tb_ag_arith"
OUTFILE="dump.txt"
LOGFILE="log.txt"

####
# Functions
####

clean()
{
    echo "Cleaning ..."
    rm -rf $WORKDIR
    rm -rf $ENTITY
    rm -rf e~$ENTITY.o
    rm -rf $OUTFILE
    rm -rf $LOGFILE
}

compile()
{
    mkdir -p $WORKDIR
    echo "Compiling..."
    if [ $# -eq 0 ]; then
        ghdl -s $GHDLFLAGS $FILES
        ghdl -a $GHDLFLAGS $FILES
        ghdl -e $GHDLFLAGS $ENTITY
    fi
}

simulate()
{
    echo "Simulating ..."
    if [ -e $ENTITY ]; then
        ./$ENTITY &> $LOGFILE
    else
        # Some GHDL versions do not produce binary.
        ghdl -r $GHDLFLAGS $ENTITY $SIMFLAGS &> $LOGFILE
    fi
}

####
# Main script
####

if [ "$1" = "sim" ]; then
    simulate
elif [ "$1" = "clean" ]; then
    clean
elif [ $# -eq 0 ]; then
    clean
    compile
else
    echo "Nothing ..."
fi
