--===========================================================================--
-- Simple wrapper for a single Cacti-P memory.
--
-- It selects the right memory instance based on the address and data width.
--
-- Supported sizes are:
--   * 32/64/128/256/512/1024/2048/4096/8192 x 32
--   * 64 x 51
--===========================================================================--

--===========================================================================--
library ieee;
use ieee.std_logic_1164.all;

entity cacti_mem_wrapper is
    generic (
        DATAW    : integer;
        ADDRW    : integer);
    port (
        clk      : in  std_logic;
        d        : in  std_logic_vector(DATAW-1 downto 0);
        addr     : in  std_logic_vector(ADDRW-1 downto 0);
        en_x     : in  std_logic;
        wr_x     : in  std_logic;
        bit_wr_x : in  std_logic_vector(DATAW-1 downto 0);
        q        : out std_logic_vector(DATAW-1 downto 0));
end cacti_mem_wrapper;

architecture struct of cacti_mem_wrapper is

    component ram_64x51
        generic (
            data_width  : integer := 51;
            addr_width  : integer := 6;
            mem_depth   : integer := 64);
        port (
            clk         : in  std_logic;
            rd_data     : out std_logic_vector(data_width-1 downto 0);
            wr_data     : in  std_logic_vector(data_width-1 downto 0);
            mem_en_x    : in  std_logic;
            addr        : in  std_logic_vector(addr_width-1 downto 0);
            bytemask_x  : in  std_logic_vector(data_width/8-1 downto 0);
            wr_en_x     : in  std_logic);
    end component;

    component ram_32x32
        generic (
            data_width  : integer := 32;
            addr_width  : integer := 5;
            mem_depth   : integer := 32);
        port (
            clk         : in  std_logic;
            rd_data     : out std_logic_vector(data_width-1 downto 0);
            wr_data     : in  std_logic_vector(data_width-1 downto 0);
            mem_en_x    : in  std_logic;
            addr        : in  std_logic_vector(addr_width-1 downto 0);
            bytemask_x  : in  std_logic_vector(data_width/8-1 downto 0);
            wr_en_x     : in  std_logic);
    end component;

    component ram_64x32
        generic (
            data_width  : integer := 32;
            addr_width  : integer := 6;
            mem_depth   : integer := 64);
        port (
            clk         : in  std_logic;
            rd_data     : out std_logic_vector(data_width-1 downto 0);
            wr_data     : in  std_logic_vector(data_width-1 downto 0);
            mem_en_x    : in  std_logic;
            addr        : in  std_logic_vector(addr_width-1 downto 0);
            bytemask_x  : in  std_logic_vector(data_width/8-1 downto 0);
            wr_en_x     : in  std_logic);
    end component;

    component ram_128x32
        generic (
            data_width  : integer := 32;
            addr_width  : integer := 7;
            mem_depth   : integer := 128);
        port (
            clk         : in  std_logic;
            rd_data     : out std_logic_vector(data_width-1 downto 0);
            wr_data     : in  std_logic_vector(data_width-1 downto 0);
            mem_en_x    : in  std_logic;
            addr        : in  std_logic_vector(addr_width-1 downto 0);
            bytemask_x  : in  std_logic_vector(data_width/8-1 downto 0);
            wr_en_x     : in  std_logic);
    end component;

    component ram_256x32
        generic (
            data_width  : integer := 32;
            addr_width  : integer := 8;
            mem_depth   : integer := 256);
        port (
            clk         : in  std_logic;
            rd_data     : out std_logic_vector(data_width-1 downto 0);
            wr_data     : in  std_logic_vector(data_width-1 downto 0);
            mem_en_x    : in  std_logic;
            addr        : in  std_logic_vector(addr_width-1 downto 0);
            bytemask_x  : in  std_logic_vector(data_width/8-1 downto 0);
            wr_en_x     : in  std_logic);
    end component;

    component ram_512x32
        generic (
            data_width  : integer := 32;
            addr_width  : integer := 9;
            mem_depth   : integer := 512);
        port (
            clk         : in  std_logic;
            rd_data     : out std_logic_vector(data_width-1 downto 0);
            wr_data     : in  std_logic_vector(data_width-1 downto 0);
            mem_en_x    : in  std_logic;
            addr        : in  std_logic_vector(addr_width-1 downto 0);
            bytemask_x  : in  std_logic_vector(data_width/8-1 downto 0);
            wr_en_x     : in  std_logic);
    end component;

    component ram_1024x32
        generic (
            data_width  : integer := 32;
            addr_width  : integer := 10;
            mem_depth   : integer := 1024);
        port (
            clk         : in  std_logic;
            rd_data     : out std_logic_vector(data_width-1 downto 0);
            wr_data     : in  std_logic_vector(data_width-1 downto 0);
            mem_en_x    : in  std_logic;
            addr        : in  std_logic_vector(addr_width-1 downto 0);
            bytemask_x  : in  std_logic_vector(data_width/8-1 downto 0);
            wr_en_x     : in  std_logic);
    end component;

    component ram_2048x32
        generic (
            data_width  : integer := 32;
            addr_width  : integer := 11;
            mem_depth   : integer := 2048);
        port (
            clk         : in  std_logic;
            rd_data     : out std_logic_vector(data_width-1 downto 0);
            wr_data     : in  std_logic_vector(data_width-1 downto 0);
            mem_en_x    : in  std_logic;
            addr        : in  std_logic_vector(addr_width-1 downto 0);
            bytemask_x  : in  std_logic_vector(data_width/8-1 downto 0);
            wr_en_x     : in  std_logic);
    end component;

    component ram_4096x32
        generic (
            data_width  : integer := 32;
            addr_width  : integer := 12;
            mem_depth   : integer := 4096);
        port (
            clk         : in  std_logic;
            rd_data     : out std_logic_vector(data_width-1 downto 0);
            wr_data     : in  std_logic_vector(data_width-1 downto 0);
            mem_en_x    : in  std_logic;
            addr        : in  std_logic_vector(addr_width-1 downto 0);
            bytemask_x  : in  std_logic_vector(data_width/8-1 downto 0);
            wr_en_x     : in  std_logic);
    end component;

    component ram_8192x32
        generic (
            data_width  : integer := 32;
            addr_width  : integer := 13;
            mem_depth   : integer := 8192);
        port (
            clk         : in  std_logic;
            rd_data     : out std_logic_vector(data_width-1 downto 0);
            wr_data     : in  std_logic_vector(data_width-1 downto 0);
            mem_en_x    : in  std_logic;
            addr        : in  std_logic_vector(addr_width-1 downto 0);
            bytemask_x  : in  std_logic_vector(data_width/8-1 downto 0);
            wr_en_x     : in  std_logic);
    end component;

    signal bytemask  : std_logic_vector(DATAW/8-1 downto 0);

begin  -- struct

    -- convert bit-write signal to bytemask
    bytemask_conv : process(bit_wr_x)
        variable i : natural;
    begin -- bytemask_conv
        for i in 0 to DATAW/8-1 loop
            -- take only the first bit of each byte
            bytemask(i) <= bit_wr_x(8*i);
        end loop;
    end process bytemask_conv;

    gen_mem_64x51 : if (ADDRW = 6) and (DATAW = 51) generate
        mem_64x51 : ram_64x51
            generic map (
                data_width  => DATAW,
                addr_width  => ADDRW ,
                mem_depth   => 2**ADDRW)
            port map (
                clk         => clk,
                rd_data     => q,
                wr_data     => d,
                mem_en_x    => en_x,
                addr        => addr,
                bytemask_x  => (others => '0'), -- ignored in 51b wide memory
                wr_en_x     => wr_x);
    end generate gen_mem_64x51;

    gen_mem_32x32 : if (ADDRW = 5) and (DATAW = 32) generate
        mem_32x32 : ram_32x32
            generic map (
                data_width  => DATAW,
                addr_width  => ADDRW,
                mem_depth   => 2**ADDRW)
            port map (
                clk         => clk,
                rd_data     => q,
                wr_data     => d,
                mem_en_x    => en_x,
                addr        => addr,
                bytemask_x  => bytemask,
                wr_en_x     => wr_x);
    end generate gen_mem_32x32;

    gen_mem_64x32 : if (ADDRW = 6) and (DATAW = 32) generate
        mem_64x32 : ram_64x32
            generic map (
                data_width  => DATAW,
                addr_width  => ADDRW,
                mem_depth   => 2**ADDRW)
            port map (
                clk         => clk,
                rd_data     => q,
                wr_data     => d,
                mem_en_x    => en_x,
                addr        => addr,
                bytemask_x  => bytemask,
                wr_en_x     => wr_x);
    end generate gen_mem_64x32;

    gen_mem_128x32 : if (ADDRW = 7) and (DATAW = 32) generate
        mem_128x32 : ram_128x32
            generic map (
                data_width  => DATAW,
                addr_width  => ADDRW,
                mem_depth   => 2**ADDRW)
            port map (
                clk         => clk,
                rd_data     => q,
                wr_data     => d,
                mem_en_x    => en_x,
                addr        => addr,
                bytemask_x  => bytemask,
                wr_en_x     => wr_x);
    end generate gen_mem_128x32;

    gen_mem_256x32 : if (ADDRW = 8) and (DATAW = 32) generate
        mem_256x32 : ram_256x32
            generic map (
                data_width  => DATAW,
                addr_width  => ADDRW,
                mem_depth   => 2**ADDRW)
            port map (
                clk         => clk,
                rd_data     => q,
                wr_data     => d,
                mem_en_x    => en_x,
                addr        => addr,
                bytemask_x  => bytemask,
                wr_en_x     => wr_x);
    end generate gen_mem_256x32;

    gen_mem_512x32 : if (ADDRW = 9) and (DATAW = 32) generate
        mem_512x32 : ram_512x32
            generic map (
                data_width  => DATAW,
                addr_width  => ADDRW,
                mem_depth   => 2**ADDRW)
            port map (
                clk         => clk,
                rd_data     => q,
                wr_data     => d,
                mem_en_x    => en_x,
                addr        => addr,
                bytemask_x  => bytemask,
                wr_en_x     => wr_x);
    end generate gen_mem_512x32;

    gen_mem_1024x32 : if (ADDRW = 10) and (DATAW = 32) generate
        mem_1024x32 : ram_1024x32
            generic map (
                data_width  => DATAW,
                addr_width  => ADDRW,
                mem_depth   => 2**ADDRW)
            port map (
                clk         => clk,
                rd_data     => q,
                wr_data     => d,
                mem_en_x    => en_x,
                addr        => addr,
                bytemask_x  => bytemask,
                wr_en_x     => wr_x);
    end generate gen_mem_1024x32;

    gen_mem_2048x32 : if (ADDRW = 11) and (DATAW = 32) generate
        mem_2048x32 : ram_2048x32
            generic map (
                data_width  => DATAW,
                addr_width  => ADDRW,
                mem_depth   => 2**ADDRW)
            port map (
                clk         => clk,
                rd_data     => q,
                wr_data     => d,
                mem_en_x    => en_x,
                addr        => addr,
                bytemask_x  => bytemask,
                wr_en_x     => wr_x);
    end generate gen_mem_2048x32;

    gen_mem_4096x32 : if (ADDRW = 12) and (DATAW = 32) generate
        mem_4096x32 : ram_4096x32
            generic map (
                data_width  => DATAW,
                addr_width  => ADDRW,
                mem_depth   => 2**ADDRW)
            port map (
                clk         => clk,
                rd_data     => q,
                wr_data     => d,
                mem_en_x    => en_x,
                addr        => addr,
                bytemask_x  => bytemask,
                wr_en_x     => wr_x);
    end generate gen_mem_4096x32;

    gen_mem_8192x32 : if (ADDRW = 13) and (DATAW = 32) generate
        mem_8192x32 : ram_8192x32
            generic map (
                data_width  => DATAW,
                addr_width  => ADDRW,
                mem_depth   => 2**ADDRW)
            port map (
                clk         => clk,
                rd_data     => q,
                wr_data     => d,
                mem_en_x    => en_x,
                addr        => addr,
                bytemask_x  => bytemask,
                wr_en_x     => wr_x);
    end generate gen_mem_8192x32;

end struct;
