--===========================================================================--
-- Memory wrapper for a synchronous dualport memory.
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- Serves as a dualport memory interface for the processor entity. It is
-- possible to use different architectures and thus change the memory
-- implementation without rewriting the proc.vhd.
--
-- Architectures:
--   struct_dualport : Standard dualport SRAM (synch_dualport_sram.vhd)
--   struct_par_mem  : Two singleport memories and a parallel memory logic
--                     module (more at:
--                       https://tce.cs.tut.fi/papers/parallel_memory.pdf)
--===========================================================================--


--===========================================================================--
-- Entity of a synchronous dualport memory wrapper
-------------------------------------------------------------------------------
-- Generics:
--   DATAW      : Width of a data word
--   ADDRW      : Number of bits required to locate all memory entries
--
-- Inputs:
--   clk        : Clock
--   d_a        : Input data, port a
--   d_b        : Input data, port b
--   addr_a     : Address, port a
--   addr_b     : Address, port b
--   en_a_x     : Memory enable, port a, active low
--   en_b_x     : Memory enable, port b, active low
--   wr_a_x     : Write enable, port a, active low
--   wr_b_x     : Write enable, port b, active low
--   bit_wr_a_x : Bit mask to selectively write bits of data word, port a,
--                active low
--   bit_wr_b_x : Bit mask to selectively write bits of data word, port b,
--                active low
--   rst_x      : Asynchronous reset (only for struct_par_mem)
--   glock      : Lock signal from processor core (only for struct_par_mem)
--
-- Outputs:
--   q_a        : Output data, port a
--   q_b        : Output data, port b
--   lockrq     : Lock signal to processor core in case of memory conflict
--                (only for struct_par_mem)
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity synch_dualport_mem_wrapper is
    generic (
        DATAW        : integer;
        ADDRW        : integer);
    port (
        clk          : in  std_logic;
        d_a          : in  std_logic_vector(DATAW-1 downto 0);
        d_b          : in  std_logic_vector(DATAW-1 downto 0);
        addr_a       : in  std_logic_vector(ADDRW-1 downto 0);
        addr_b       : in  std_logic_vector(ADDRW-1 downto 0);
        en_a_x       : in  std_logic;
        en_b_x       : in  std_logic;
        wr_a_x       : in  std_logic;
        wr_b_x       : in  std_logic;
        bit_wr_a_x   : in  std_logic_vector(DATAW-1 downto 0);
        bit_wr_b_x   : in  std_logic_vector(DATAW-1 downto 0);
        q_a          : out std_logic_vector(DATAW-1 downto 0);
        q_b          : out std_logic_vector(DATAW-1 downto 0);
        -- Only needed for struct_par_mem architecture
        rst_x        : in  std_logic;
        glock        : in  std_logic;
        lockrq       : out std_logic);
end synch_dualport_mem_wrapper;
--===========================================================================--

--===========================================================================--
-- Architecture struct_dualport
--
-- Dualport memory implemented as traditional synchronous dualport SRAM
-------------------------------------------------------------------------------
architecture struct_dualport of synch_dualport_mem_wrapper is

    component synch_dualport_sram
        generic (
            DATAW        : integer;
            ADDRW        : integer);
        port (
            clk          : in  std_logic;
            d_a          : in  std_logic_vector(DATAW-1 downto 0);
            d_b          : in  std_logic_vector(DATAW-1 downto 0);
            addr_a       : in  std_logic_vector(ADDRW-1 downto 0);
            addr_b       : in  std_logic_vector(ADDRW-1 downto 0);
            en_a_x       : in  std_logic;
            en_b_x       : in  std_logic;
            wr_a_x       : in  std_logic;
            wr_b_x       : in  std_logic;
            bit_wr_a_x   : in  std_logic_vector(DATAW-1 downto 0);
            bit_wr_b_x   : in  std_logic_vector(DATAW-1 downto 0);
            q_a          : out std_logic_vector(DATAW-1 downto 0);
            q_b          : out std_logic_vector(DATAW-1 downto 0));
    end component;

begin -- struct_dualport

    mem : synch_dualport_sram
        generic map (
            DATAW      => DATAW,
            ADDRW      => ADDRW)
        port map (
            clk        => clk,
            d_a        => d_a,
            d_b        => d_b,
            addr_a     => addr_a,
            addr_b     => addr_b,
            en_a_x     => en_a_x,
            en_b_x     => en_b_x,
            wr_a_x     => wr_a_x,
            wr_b_x     => wr_b_x,
            bit_wr_a_x => bit_wr_a_x,
            bit_wr_b_x => bit_wr_b_x,
            q_a        => q_a,
            q_b        => q_b);

    lockrq <= '0';

end struct_dualport;
--===========================================================================--

--===========================================================================--
-- Architecture struct_par_mem
--
-- Dualport memory implemented as two singleport memories. par_mem_logic is
-- inserted between processor's LSUs and the memories to prevent memory
-- conflicts (i.e. writing to a same address in both ports in one clock cycle).
--
-- par_mem_logic module (and package) is made by Jarno K. Tanskanen at TUT.
--
-- More info at: https://tce.cs.tut.fi/papers/parallel_memory.pdf
-------------------------------------------------------------------------------
use work.proc_constants.all;
use work.par_mem_pkg.all;

architecture struct_par_mem of synch_dualport_mem_wrapper is

    component synch_singleport_mem_wrapper
        generic (
            DATAW    : integer;
            ADDRW    : integer);
        port (
            clk      : in  std_logic;
            rst_x    : in  std_logic;
            glock    : in  std_logic;
            d        : in  std_logic_vector(DATAW-1 downto 0);
            addr     : in  std_logic_vector(ADDRW-1 downto 0);
            en_x     : in  std_logic;
            wr_x     : in  std_logic;
            bit_wr_x : in  std_logic_vector(DATAW-1 downto 0);
            q        : out std_logic_vector(DATAW-1 downto 0));
    end component;

    component par_mem_logic
        generic(
            PORTS     : integer := 2;
            LSU_ADDRW : integer := 8; -- Likely equals to DMEMADDRWIDTH.
            CTRLW     : integer := 1; -- Number of bits required for PORTS.
            DATAW     : integer := 8; -- Likely equals to DMEMDATAWIDTH in
                                      -- globals_pkg.vhdl
            PM_FUNC   : par_mem_func);
        port(
            -- Connections for the side of the LSUs.
            lsu_en_xs         : in  std_logic_vector(PORTS-1 downto 0);
            lsu_wr_xs         : in  std_logic_vector(PORTS-1 downto 0);
            addrs_from_lsus   : in  std_logic_vector(PORTS*LSU_ADDRW-1
                                                     downto 0);
            st_data_from_lsus : in  std_logic_vector(PORTS*DATAW-1 downto 0);
            ld_data_to_lsus   : out std_logic_vector(PORTS*DATAW-1 downto 0);
            -- Connections for the side of the memory modules.
            mem_en_xs         : out std_logic_vector(PORTS-1 downto 0);
            mem_wr_xs         : out std_logic_vector(PORTS-1 downto 0);
            addrs_to_mems     : out std_logic_vector(PORTS*(LSU_ADDRW-CTRLW)-1
                                                 downto 0);
            st_data_to_mems   : out std_logic_vector(PORTS*DATAW-1 downto 0);
            ld_data_from_mems : in  std_logic_vector(PORTS*DATAW-1 downto 0);
            -- Other important signals.
            clk               : in  std_logic;
            rstx              : in  std_logic;
            glock             : in  std_logic;
            lockrq            : out std_logic);
    end component;

    component par_mem_scheduler
        generic (
            dataw        : integer := 32;
            addrw        : integer := 16;
            ports        : integer := 2;
            rd_delay     : integer := 1);
        port (
            -- Inputs from LSUs
            lsu_d_a      : in  std_logic_vector(dataw-1 downto 0);
            lsu_d_b      : in  std_logic_vector(dataw-1 downto 0);
            lsu_addr_a   : in  std_logic_vector(addrw-1 downto 0);
            lsu_addr_b   : in  std_logic_vector(addrw-1 downto 0);
            lsu_en_a     : in  std_logic;
            lsu_en_b     : in  std_logic;
            lsu_wr_a     : in  std_logic;
            lsu_wr_b     : in  std_logic;
            lsu_bit_wr_a : in  std_logic_vector(dataw-1 downto 0);
            lsu_bit_wr_b : in  std_logic_vector(dataw-1 downto 0);
            -- Outputs to par_mem
            d            : out std_logic_vector(ports*dataw-1 downto 0);
            addr         : out std_logic_vector(ports*addrw-1 downto 0);
            en           : out std_logic_vector(1 downto 0);
            wr           : out std_logic_vector(1 downto 0);
            bit_wr       : out std_logic_vector(ports*dataw-1 downto 0);
            -- Inputs from par_mem
            q            : in  std_logic_vector(ports*dataw-1 downto 0);
            -- Outputs to LSUs
            lsu_q_a      : out std_logic_vector(dataw-1 downto 0);
            lsu_q_b      : out std_logic_vector(dataw-1 downto 0);
            -- Clock, reset and lock
            clk          : in  std_logic;
            rst_x        : in  std_logic;
            glock        : in  std_logic);
    end component;

    -- Rename DATAW and ADDRW to avoid confusion
    constant DATA_WIDTH : integer := DATAW;
    constant ADDR_WIDTH : integer := ADDRW;
    constant MEM_ADDRW  : integer := ADDR_WIDTH-1;

    -- par_mem scheduler's (LSU's)side
    signal lsu_d        : std_logic_vector(2*DATA_WIDTH-1 downto 0);
    signal lsu_addr     : std_logic_vector(2*ADDR_WIDTH-1 downto 0);
    signal lsu_en       : std_logic_vector(1 downto 0);
    signal lsu_wr       : std_logic_vector(1 downto 0);
    signal lsu_q        : std_logic_vector(2*DATA_WIDTH-1 downto 0);
    signal lsu_bit_wr   : std_logic_vector(2*DATA_WIDTH-1 downto 0);
    signal q_a_out      : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal q_b_out      : std_logic_vector(DATA_WIDTH-1 downto 0);

    -- memory modules' side
    signal par_mem_en_x : std_logic_vector(1 downto 0);
    signal par_mem_wr_x : std_logic_vector(1 downto 0);
    signal par_mem_addr : std_logic_vector(2*MEM_ADDRW-1 downto 0);
    signal par_mem_d    : std_logic_vector(2*DATA_WIDTH-1 downto 0);
    signal par_mem_q    : std_logic_vector(2*DATA_WIDTH-1 downto 0);

    signal par_mem_lock_rq : std_logic;
    signal lockrq_reg      : std_logic;
    signal par_mem_glock   : std_logic;

begin -- struct_par_mem

    -- Register lock request from par_mem (synchronize with glock)
    reg : process(clk, rst_x)
    begin -- reg
        if rst_x = '0' then
            lockrq_reg <= '0';
        end if;
        if rising_edge(clk) then
            if par_mem_glock = '0' then
                lockrq_reg <= par_mem_lock_rq;
            end if;
        end if;
    end process reg;

    -- If there is a lock request from par_mem, ignore the glock
    resolve_glock : process(glock, lockrq_reg)
    begin -- resolve_glock
        if (glock = '1') and (lockrq_reg = '0') then
            par_mem_glock <= '1';
        else
            par_mem_glock <= '0';
        end if;
    end process resolve_glock;

    -- Serial-parallel scheduling
    gen_scheduler_parallel : if PAR_MEM_ACCESS_CONF = Parallel generate
        scheduler : entity work.par_mem_scheduler(parallel_access)
            generic map (
                dataw        => DATA_WIDTH,
                addrw        => ADDR_WIDTH,
                ports        => 2,
                rd_delay     => 1)
            port map (
                lsu_d_a      => d_a,
                lsu_d_b      => d_b,
                lsu_addr_a   => addr_a,
                lsu_addr_b   => addr_b,
                lsu_en_a     => en_a_x,
                lsu_en_b     => en_b_x,
                lsu_wr_a     => wr_a_x,
                lsu_wr_b     => wr_b_x,
                lsu_bit_wr_a => bit_wr_a_x,
                lsu_bit_wr_b => bit_wr_a_x,
                q            => lsu_q,
                lsu_q_a      => q_a_out,
                lsu_q_b      => q_b_out,
                d            => lsu_d,
                addr         => lsu_addr,
                en           => lsu_en,
                wr           => lsu_wr,
                bit_wr       => lsu_bit_wr,
                clk          => clk,
                rst_x        => rst_x,
                glock        => glock);
    end generate gen_scheduler_parallel;

    gen_scheduler_direct : if PAR_MEM_ACCESS_CONF = Direct generate
        scheduler : entity work.par_mem_scheduler(direct)
            generic map (
                dataw        => DATA_WIDTH,
                addrw        => ADDR_WIDTH,
                ports        => 2,
                rd_delay     => 1)
            port map (
                lsu_d_a      => d_a,
                lsu_d_b      => d_b,
                lsu_addr_a   => addr_a,
                lsu_addr_b   => addr_b,
                lsu_en_a     => en_a_x,
                lsu_en_b     => en_b_x,
                lsu_wr_a     => wr_a_x,
                lsu_wr_b     => wr_b_x,
                lsu_bit_wr_a => bit_wr_a_x,
                lsu_bit_wr_b => bit_wr_a_x,
                q            => lsu_q,
                lsu_q_a      => q_a_out,
                lsu_q_b      => q_b_out,
                d            => lsu_d,
                addr         => lsu_addr,
                en           => lsu_en,
                wr           => lsu_wr,
                bit_wr       => lsu_bit_wr,
                clk          => clk,
                rst_x        => rst_x,
                glock        => glock);
    end generate gen_scheduler_direct;

    par_mem_logic_inst : par_mem_logic
        generic map (
            PORTS             => 2,
            LSU_ADDRW         => ADDR_WIDTH,
            CTRLW             => 1,
            DATAW             => DATA_WIDTH,
            PM_FUNC           => FFT_STRIDE_2_PORT)
        port map (
            lsu_en_xs         => lsu_en,
            lsu_wr_xs         => lsu_wr,
            addrs_from_lsus   => lsu_addr,
            st_data_from_lsus => lsu_d,
            ld_data_to_lsus   => lsu_q,
            mem_en_xs         => par_mem_en_x,
            mem_wr_xs         => par_mem_wr_x,
            addrs_to_mems     => par_mem_addr,
            st_data_to_mems   => par_mem_d,
            ld_data_from_mems => par_mem_q,
            clk               => clk,
            rstx              => rst_x,
            glock             => par_mem_glock,
            lockrq            => par_mem_lock_rq);

    gen_dmem_1_cacti : if SP_MEM_CONF = Cacti generate
        dmem_1 : entity work.synch_singleport_mem_wrapper(struct_cacti)
            generic map (
                DATAW    => DATA_WIDTH,
                ADDRW    => MEM_ADDRW)
            port map (
                clk      => clk,
                rst_x    => rst_x,
                glock    => par_mem_glock,
                d        => par_mem_d(2*DATA_WIDTH-1 downto DATA_WIDTH),
                addr     => par_mem_addr(2*MEM_ADDRW-1 downto MEM_ADDRW),
                en_x     => par_mem_en_x(1),
                wr_x     => par_mem_wr_x(1),
                bit_wr_x => bit_wr_a_x,
                q        => par_mem_q(2*DATA_WIDTH-1 downto DATA_WIDTH));
    end generate gen_dmem_1_cacti;

    gen_dmem_1_cacti_blocks : if SP_MEM_CONF = CactiBlocks generate
        dmem_1 : entity work.synch_singleport_mem_wrapper(struct_cacti_blocks)
            generic map (
                DATAW    => DATA_WIDTH,
                ADDRW    => MEM_ADDRW)
            port map (
                clk      => clk,
                rst_x    => rst_x,
                glock    => par_mem_glock,
                d        => par_mem_d(2*DATA_WIDTH-1 downto DATA_WIDTH),
                addr     => par_mem_addr(2*MEM_ADDRW-1 downto MEM_ADDRW),
                en_x     => par_mem_en_x(1),
                wr_x     => par_mem_wr_x(1),
                bit_wr_x => bit_wr_a_x,
                q        => par_mem_q(2*DATA_WIDTH-1 downto DATA_WIDTH));
    end generate gen_dmem_1_cacti_blocks;

    gen_dmem_1_singleport : if SP_MEM_CONF = Singleport generate
        dmem_1 : entity work.synch_singleport_mem_wrapper(struct_singleport)
            generic map (
                DATAW    => DATA_WIDTH,
                ADDRW    => MEM_ADDRW)
            port map (
                clk      => clk,
                rst_x    => rst_x,
                glock    => par_mem_glock,
                d        => par_mem_d(2*DATA_WIDTH-1 downto DATA_WIDTH),
                addr     => par_mem_addr(2*MEM_ADDRW-1 downto MEM_ADDRW),
                en_x     => par_mem_en_x(1),
                wr_x     => par_mem_wr_x(1),
                bit_wr_x => bit_wr_a_x,
                q        => par_mem_q(2*DATA_WIDTH-1 downto DATA_WIDTH));
    end generate gen_dmem_1_singleport;

    gen_dmem_2_cacti : if SP_MEM_CONF = Cacti generate
        dmem_2 : entity work.synch_singleport_mem_wrapper(struct_cacti)
            generic map (
                DATAW    => DATA_WIDTH,
                ADDRW    => MEM_ADDRW)
            port map (
                clk      => clk,
                rst_x    => rst_x,
                glock    => par_mem_glock,
                d        => par_mem_d(DATA_WIDTH-1 downto 0),
                addr     => par_mem_addr(MEM_ADDRW-1 downto 0),
                en_x     => par_mem_en_x(0),
                wr_x     => par_mem_wr_x(0),
                bit_wr_x => bit_wr_b_x,
                q        => par_mem_q(DATA_WIDTH-1 downto 0));
    end generate gen_dmem_2_cacti;

    gen_dmem_2_cacti_blocks : if SP_MEM_CONF = CactiBlocks generate
        dmem_2 : entity work.synch_singleport_mem_wrapper(struct_cacti_blocks)
            generic map (
                DATAW    => DATA_WIDTH,
                ADDRW    => MEM_ADDRW)
            port map (
                clk      => clk,
                rst_x    => rst_x,
                glock    => par_mem_glock,
                d        => par_mem_d(DATA_WIDTH-1 downto 0),
                addr     => par_mem_addr(MEM_ADDRW-1 downto 0),
                en_x     => par_mem_en_x(0),
                wr_x     => par_mem_wr_x(0),
                bit_wr_x => bit_wr_b_x,
                q        => par_mem_q(DATA_WIDTH-1 downto 0));
    end generate gen_dmem_2_cacti_blocks;

    gen_dmem_2_singleport : if SP_MEM_CONF = Singleport generate
        dmem_2 : entity work.synch_singleport_mem_wrapper(struct_singleport)
            generic map (
                DATAW    => DATA_WIDTH,
                ADDRW    => MEM_ADDRW)
            port map (
                clk      => clk,
                rst_x    => rst_x,
                glock    => par_mem_glock,
                d        => par_mem_d(DATA_WIDTH-1 downto 0),
                addr     => par_mem_addr(MEM_ADDRW-1 downto 0),
                en_x     => par_mem_en_x(0),
                wr_x     => par_mem_wr_x(0),
                bit_wr_x => bit_wr_b_x,
                q        => par_mem_q(DATA_WIDTH-1 downto 0));
    end generate gen_dmem_2_singleport;

    -- Outputs
    q_a    <= q_a_out;
    q_b    <= q_b_out;
    lockrq <= par_mem_lock_rq;

end struct_par_mem;
--===========================================================================--
