-- This module controls how par_mem_logic is accessed.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity par_mem_scheduler is
    generic (
        dataw        : integer := 32;
        addrw        : integer := 16;
        ports        : integer := 2;  -- only 2 is supported atm
        rd_delay     : integer := 1); -- only 1 is supported atm
    port (
        -- Inputs from LSUs
        lsu_d_a      : in  std_logic_vector(dataw-1 downto 0);
        lsu_d_b      : in  std_logic_vector(dataw-1 downto 0);
        lsu_addr_a   : in  std_logic_vector(addrw-1 downto 0);
        lsu_addr_b   : in  std_logic_vector(addrw-1 downto 0);
        lsu_en_a     : in  std_logic;
        lsu_en_b     : in  std_logic;
        lsu_wr_a     : in  std_logic;
        lsu_wr_b     : in  std_logic;
        lsu_bit_wr_a : in  std_logic_vector(dataw-1 downto 0);
        lsu_bit_wr_b : in  std_logic_vector(dataw-1 downto 0);
        -- Outputs to par_mem
        d            : out std_logic_vector(ports*dataw-1 downto 0);
        addr         : out std_logic_vector(ports*addrw-1 downto 0);
        en           : out std_logic_vector(ports-1 downto 0);
        wr           : out std_logic_vector(ports-1 downto 0);
        bit_wr       : out std_logic_vector(ports*dataw-1 downto 0);
        -- Inputs from par_mem
        q            : in  std_logic_vector(ports*dataw-1 downto 0);
        -- Outputs to LSUs
        lsu_q_a      : out std_logic_vector(dataw-1 downto 0);
        lsu_q_b      : out std_logic_vector(dataw-1 downto 0);
        -- Clock, reset and lock
        clk          : in  std_logic;
        rst_x        : in  std_logic;
        glock        : in  std_logic);
end par_mem_scheduler;


architecture direct of par_mem_scheduler is
begin -- direct

    -- Inputs
    d       <= lsu_d_a      & lsu_d_b;
    addr    <= lsu_addr_a   & lsu_addr_b;
    en      <= lsu_en_a     & lsu_en_b;
    wr      <= lsu_wr_a     & lsu_wr_b;
    bit_wr  <= lsu_bit_wr_a & lsu_bit_wr_b;
    -- Outputs
    lsu_q_a <= q(2*dataw-1 downto dataw);
    lsu_q_b <= q(dataw-1 downto 0);

end direct;


architecture parallel_access of par_mem_scheduler is

    component ser_par_conv
        generic (
            portw    : integer;
            port_cnt : integer);
        port (
            data_in  : in  std_logic_vector(portw-1 downto 0);
            data_out : out std_logic_vector(port_cnt*portw-1 downto 0);
            en       : in  std_logic;  -- active low
            en_out   : out std_logic_vector(port_cnt-1 downto 0);
            clk      : in  std_logic;
            rst_x    : in  std_logic;  -- active low
            glock    : in  std_logic); -- active high
    end component;

    component par_ser_conv
        generic (
            portw    : integer := 32;
            port_cnt : integer := 2);
        port (
            data_in  : in  std_logic_vector(port_cnt*portw-1 downto 0);
            data_out : out std_logic_vector(portw-1 downto 0);
            load     : in  std_logic;  -- active low
            clk      : in  std_logic;
            rst_x    : in  std_logic;  -- active low
            glock    : in  std_logic); -- active high
    end component;

    signal d_a         : std_logic_vector(ports*dataw-1 downto 0);
    signal addr_a      : std_logic_vector(ports*addrw-1 downto 0);
    signal en_a        : std_logic_vector(ports-1 downto 0);
    signal wr_a        : std_logic_vector(ports-1 downto 0);
    signal bit_wr_a    : std_logic_vector(ports*dataw-1 downto 0);

    signal d_b         : std_logic_vector(ports*dataw-1 downto 0);
    signal addr_b      : std_logic_vector(ports*addrw-1 downto 0);
    signal en_b        : std_logic_vector(ports-1 downto 0);
    signal wr_b        : std_logic_vector(ports-1 downto 0);
    signal bit_wr_b    : std_logic_vector(ports*dataw-1 downto 0);

    signal ctrl        : std_logic_vector(2*ports-1 downto 0);

    signal rd_load     : std_logic;
    signal rd_load_reg : std_logic;
    signal rd_port     : std_logic;

    signal q_a_out     : std_logic_vector(dataw-1 downto 0);
    signal q_b_out     : std_logic_vector(dataw-1 downto 0);

    signal DEBUG_problem_cnt : integer;

begin -- parallel_access

    -- Inputs from a-port
    addr_a_conv : ser_par_conv
        generic map (
            portw    => lsu_addr_a'length,
            port_cnt => ports)
        port map (
            data_in  => lsu_addr_a,
            data_out => addr_a,
            en       => lsu_en_a,
            en_out   => en_a,
            clk      => clk,
            rst_x    => rst_x,
            glock    => glock);

    d_a_conv : ser_par_conv
        generic map (
            portw    => lsu_d_a'length,
            port_cnt => ports)
        port map (
            data_in  => lsu_d_a,
            data_out => d_a,
            en       => lsu_wr_a,
            en_out   => wr_a,
            clk      => clk,
            rst_x    => rst_x,
            glock    => glock);

    bit_wr_a_conv : ser_par_conv
        generic map (
            portw    => lsu_bit_wr_a'length,
            port_cnt => ports)
        port map (
            data_in  => lsu_bit_wr_a,
            data_out => bit_wr_a,
            en       => lsu_wr_a,
            -- en_out   => , -- unconnected
            clk      => clk,
            rst_x    => rst_x,
            glock    => glock);

    -- Inputs from b-port
    addr_b_conv : ser_par_conv
        generic map (
            portw    => lsu_addr_b'length,
            port_cnt => ports)
        port map (
            data_in  => lsu_addr_b,
            data_out => addr_b,
            en       => lsu_en_b,
            en_out   => en_b,
            clk      => clk,
            rst_x    => rst_x,
            glock    => glock);

    d_b_conv : ser_par_conv
        generic map (
            portw    => lsu_d_b'length,
            port_cnt => ports)
        port map (
            data_in  => lsu_d_b,
            data_out => d_b,
            en       => lsu_wr_b,
            en_out   => wr_b,
            clk      => clk,
            rst_x    => rst_x,
            glock    => glock);

    bit_wr_b_conv : ser_par_conv
        generic map (
            portw    => lsu_bit_wr_b'length,
            port_cnt => ports)
        port map (
            data_in  => lsu_bit_wr_b,
            data_out => bit_wr_b,
            en       => lsu_wr_b,
            -- en_out   => , -- unconnected
            clk      => clk,
            rst_x    => rst_x,
            glock    => glock);

    ctrl <= en_a & en_b;

    -- Resolve outputs to par_mem; combinatorial
    out_resolve : process(d_a, addr_a, wr_a, bit_wr_a,
                          d_b, addr_b, wr_b, bit_wr_b,
                          en_a, en_b, ctrl)
        variable nulls : std_logic_vector(ports-1 downto 0);
        variable ones  : std_logic_vector(ports-1 downto 0);
    begin -- out_resolve
        nulls := (others => '0');
        ones  := (others => '1');

        if ctrl = (ones & nulls) then -- port b active
            d      <= d_b;
            addr   <= addr_b;
            wr     <= wr_b;
            en     <= en_b;
            bit_wr <= bit_wr_b;
        else -- other cases, port a takes precedence
            d      <= d_a;
            addr   <= addr_a;
            wr     <= wr_a;
            en     <= en_a;
            bit_wr <= bit_wr_a;
        end if;
    end process out_resolve;

    par_ser_conv_inst : par_ser_conv
        generic map (
            portw    => dataw,
            port_cnt => 2)
        port map (
            data_in  => q,
            data_out => q_a_out, -- Assuming only port a is reading
            load     => rd_load_reg,
            clk      => clk,
            rst_x    => rst_x,
            glock    => glock);

    -- Load signal register
    load_reg : process (clk, rst_x)
    begin -- load_reg
        if rst_x = '0' then
            rd_load_reg <= '1';
            DEBUG_problem_cnt <= 0;
        elsif rising_edge(clk) then
            if glock = '0' then
                rd_load_reg <= rd_load;
                if (lsu_addr_a = lsu_addr_b) and (lsu_en_a = '0') and
                                                 (lsu_en_b = '0') then
                    DEBUG_problem_cnt <= DEBUG_problem_cnt + 1;
                end if;
            end if;
        end if;
    end process load_reg;

    -- Resolve load signal (assuming port a is the read port)
    load_resolve : process(en_a, wr_a)
    begin -- load_resolve
        rd_load <= en_a(0) or not wr_a(0);
    end process load_resolve;

    q_b_out <= (others => '0');
    lsu_q_a <= q_a_out;
    lsu_q_b <= q_b_out;

end parallel_access;
