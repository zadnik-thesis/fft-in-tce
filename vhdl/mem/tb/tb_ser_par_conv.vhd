library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_ser_par_conv is
    -- port ( );
end tb_ser_par_conv;

architecture testbench of tb_ser_par_conv is

    constant portw    : integer := 8;
    constant port_cnt : integer := 2;

    component ser_par_conv
        generic (
            portw    : integer;
            port_cnt : integer);
        port (
            data_in  : in  std_logic_vector(portw-1 downto 0);
            data_out : out std_logic_vector(port_cnt*portw-1 downto 0);
            clk      : in  std_logic;
            en       : in  std_logic;  -- active low
            rst_x    : in  std_logic;  -- active low
            glock    : in  std_logic); -- active high
    end component;

    signal d : std_logic_vector(portw-1 downto 0)          := (others => '0');
    signal q : std_logic_vector(port_cnt*portw-1 downto 0) := (others => '0');
    signal en    : std_logic := '1';
    signal rstx  : std_logic := '0';
    signal glock : std_logic := '1';
    signal clk   : std_logic := '0';
    constant clk_period : time      := 10 ns;

begin -- testbench

    -- device under test
    dut : ser_par_conv
        generic map (
            portw    => portw,
            port_cnt => port_cnt)
        port map (
            data_in  => d,
            data_out => q,
            clk      => clk,
            en       => en,
            rst_x    => rstx,
            glock    => glock);

    -- clock signal
    clk <= not clk after clk_period/2;

    -- stimulus
    stim_proc : process
        variable i : integer := 0;
    begin -- stim_proc

        wait until rising_edge(clk);
        glock <= '0';
        rstx  <= '1';
        wait until rising_edge(clk);

        while i < 15 loop
            en <= '0';
            d <= std_logic_vector(to_unsigned(i, d'length));
            i := i + 1;
            wait until rising_edge(clk);
        end loop;

        en <= '1';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);

    end process stim_proc;

end testbench;
