--===========================================================================--
-- All outputs from the memory blocks are fed into this device. The output of
-- the whole memory is then chosen based on the memory enable signal fed into
-- the blocks.
--
-- The selector assumes that only one memory block was activated at a time.
--===========================================================================--

--===========================================================================--
-- Entity of block_mem_output_sel
-------------------------------------------------------------------------------
-- Generics:
--   DATAW       : Width of the data word
--   NBLOCKS     : Number of blocks the memory is split into
--
-- Inputs:
--   en_blocks   : Memory enable signals sent to the blocks
--   mem_outputs : Outputs from the memory blocks
--
-- Outputs:
--   mem_out     : Data word selected from `mem_outputs`
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

use work.mem_constants.all;

entity block_mem_output_sel is
    generic (
        DATAW       : positive;
        NBLOCKS     : positive);
    port (
        clk         : in  std_logic;
        rst_x       : in  std_logic;
        glock       : in  std_logic;
        en_blocks   : in  std_logic_vector(NBLOCKS-1 downto 0);
        mem_outputs : in  block_mem_outputs(NBLOCKS-1 downto 0);
        mem_out     : out std_logic_vector(DATAW-1 downto 0));
end block_mem_output_sel;
--===========================================================================--

--===========================================================================--
-- Architecture Behavioral
-------------------------------------------------------------------------------
architecture Behavioral of block_mem_output_sel is

    signal en_blocks_reg : std_logic_vector(NBLOCKS-1 downto 0);

begin -- Behavioral

    -- delay enable signals by 1 cycle
    reg : process(clk, rst_x)
    begin --reg
        if rst_x = '0' then
            en_blocks_reg <= (others => '1');
        elsif rising_edge(clk) then
            if glock = '0' then
                en_blocks_reg <= en_blocks;
            end if;
        end if;
    end process reg;

    -- select the right output
    output_sel : process(mem_outputs, en_blocks_reg)
        variable i : natural;
    begin
        mem_out <= (others => '0');
        for i in 0 to NBLOCKS-1 loop
            if en_blocks_reg(i) = '0' then
                mem_out <= mem_outputs(i);
            end if;
        end loop;
    end process output_sel;

end Behavioral;
--===========================================================================--
