--===========================================================================--
-- Constants and types used in memory-related files
--===========================================================================--
library ieee;
use ieee.std_logic_1164.all;

use work.proc_constants.all;

package mem_constants is

    -- Size of the smallest memory block (2^BLOCK_MIN_ADDRW x DMEMDATAWIDTH)
    constant BLOCK_MIN_ADDRW : positive := 5;

    -- Uniting all memory block outputs into one type
    type block_mem_outputs is array (integer range <>) of
        std_logic_vector(DMEMDATAWIDTH-1 downto 0);

end mem_constants;
