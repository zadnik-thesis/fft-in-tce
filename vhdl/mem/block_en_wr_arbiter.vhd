--===========================================================================--
-- Arbiter for enable and write-enable signals to memory blocks
--
-- It is used when a big single-port memory is split into smaller memory
-- blocks to save power. This block routes the `en` and `wr` signals into an
-- appropriate memory block (based on the address) while other blocks are
-- turned off.
--
-- The block is selected based on how many address MSBs are '0's.
--
-- The memory blocks are assumed to follow this size pattern:
--   min_size, min_size, min_size*2, min_size*4, ... , 2^(ADDRW-1)
-- When added together, the total size is 2^ADDRW.
--===========================================================================--

--===========================================================================--
-- Entity of block_en_wr_arbiter
-------------------------------------------------------------------------------
-- Generics:
--   ADDRW     : Address width of the whole memory
--   NBLOCKS   : Number of blocks the memory is split into
--
-- Inputs:
--   en        : Enable signal
--   wr        : Write enable signal
--   addr      : Original address
--
-- Outputs:
--   en_blocks : Sending `en` into a chosen memory block. Other blocks are
--               supplied by '1'. MSB is for largest memory, LSB for smallest.
--   wr_blocks : Same as `en_blocks` but for the `wr` signal.
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity block_en_wr_arbiter is
    generic (
        ADDRW     : positive;
        NBLOCKS   : positive);
    port (
        en        : in  std_logic;
        wr        : in  std_logic;
        addr      : in  std_logic_vector(ADDRW-1 downto 0);
        en_blocks : out std_logic_vector(NBLOCKS-1 downto 0);
        wr_blocks : out std_logic_vector(NBLOCKS-1 downto 0));
end block_en_wr_arbiter;
--===========================================================================--

--===========================================================================--
-- Architecture Behavioral
-------------------------------------------------------------------------------
architecture Behavioral of block_en_wr_arbiter is

begin -- Behavioral

    en_wr_select : process(en, wr, addr)
        variable i : natural;
    begin -- en_wr_select
        en_blocks <= (others => '1');
        en_blocks(en_blocks'high) <= en;
        wr_blocks <= (others => '1');
        wr_blocks(wr_blocks'high) <= wr;
        for i in 0 to NBLOCKS-2 loop
            if addr(addr'high downto addr'high-i) =
                   (addr'high downto addr'high-i => '0') then
                en_blocks <= (others => '1');
                en_blocks(en_blocks'high-i-1) <= en;
                wr_blocks <= (others => '1');
                wr_blocks(wr_blocks'high-i-1) <= wr;
            end if;
        end loop;
    end process en_wr_select;

end Behavioral;
--===========================================================================--
